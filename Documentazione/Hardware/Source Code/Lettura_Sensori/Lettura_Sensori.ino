#include <math.h>    // libreria per il logaritmo
#include <dht11.h> //includo la libreria
#include <SFE_BMP180.h> // includiamo la libreria del sensore
#include <Wire.h>  // includiamo la libreria wire per la comunicazioneI2C

#define PRESSIONE_MARE 1013.25 // pressione sul livello del mare in mb
#define v_In 5.00    // Tensione di ingresso 5 V
#define r_2 10000.00  // Definiamo la resistenza da 10k del partitore  
#define DHT11_PIN 4 // definisco la costante del pin digitale dove fare la lettura
#define analogInPin A0 //definiamo il pin analogico
dht11 DHT11 ; // istanzio un oggetto di tale classe
SFE_BMP180 pressure; // istanziamo un oggetto
int chk =0;     // imposto una variabile di tipo integer (chk) in cui memorizzerai i valori restituiti dall’interrogazione della sonda;
int sensorValue ;  // Definiamo il valore in digitale che viene letto dal pin analogico
double v_BC;          // Definiamo la tensione letta sulla resistenza da 10 k presente nel partitore di tensione con l'LDR
double r_LDR ;      // Inizializziamo la resistenza dell'LDR   
double fc ;        // Inizializziamo le fotocandele della luminosità



void setup() {
  Serial.begin(9600);            //Inizializzo il monitor seriale a una trasmissione di 9600 baud
  pinMode(analogInPin, INPUT);    // Inizializzo il pin A0 come INPUT
  Serial.println("Sensore \tStato \tUmidita'(%) \tTemperatura(C) \tLuminosita'(Fc)\tPressione (Pa)\tAltitudine(m)");
}
 
void loop() {
         Serial.print("DHT11\t\t");
          chk = DHT11.read(DHT11_PIN);    // leggo i dati del sensore
          switch (chk){                  // la variabile chk mi indica lo stato del sensore
          case DHTLIB_OK:              // se la variabile vale 0, che è il valore della costante DHTLIB_OK allora il sensore campiona e funziona correttamente             
                      Serial.print(" OK \t\t"); 
                      Serial.print(DHT11.humidity, 1); // risoluzione sensore 1%
                      Serial.print("\t\t  ");
                      Serial.println(DHT11.temperature, 1);  // risoluzone sensore 1 grado, per questo metto 1 dopo println            
                      break;
          case DHTLIB_ERROR_CHECKSUM: // se la variabile vale -1, che è il valore della costante DHTLIB_ERROR allora il sensore non passa la fase di test e i dati nn sono corretti
                      Serial.println("Errore nei dati\t"); 
                      break;
          case DHTLIB_ERROR_TIMEOUT: // se la variabile vale -2, che è il valore della costante DHTLIB_ERROR_T allora il sensore non comunica correttamente
                      Serial.println("Errore nella comunicazione\t"); 
                      break;
          default: 
                      Serial.println("Errore sconosciuto,\t"); 
                      break;
  }

 
   

  Serial.print("LDR\t\t");
  sensorValue = analogRead(analogInPin);                      // Vado a leggere il valore di A0
 
  v_BC=( ( v_In/1024 )* ((double) sensorValue));              // Converto questo valore letto sensorValue sul pin A0( che è un valore tra 0 e 1023( in quanto si ha una risoluzione a 8 bit)) in Volt
                                                              // dove ogni singolo passo di quantizzazione corrisponde a 4,9 mV (cioè a 5/1024)
  r_LDR = (r_2 * v_In/v_BC )- r_2;                           // Applicando il partitore mi ricavo la resistenza dell'LDR attraverso questa espressione
  
  fc = pow(10,(log10(r_LDR) - 4.7 )/ (-0.86));                // attraverso questa espressione che riporteremo per intero nell'analisi circuitale dei sensori nella relazione, mi ricavo le fotocandele dalla r_LDR
  
  Serial.print("\t\t\t\t\t\t ");
  Serial.println(fc);
  
  if (pressure.begin()){
    Serial.print("BMP180");
    Serial.print("\t\t OK");
    }
  else
  {
    //probabilmente c'è qualche collegamento fatto male e quindi non riesco a inizializzarlo correttamente il sensore

    Serial.println("BMP180 inizializzazione fallita\n\n");
    return;
  }
  
  char status;
  double T,P,a;

 
  
 
  // tale sensore funziona correttamente andando a misurare prima la temperatura e poi la pressione(datasheet)
  // Misura di temperatura
  // Iniziamo a misurare la temperatura. Se startTemperature ritorna un valore diverso da 0, allora la misura è iniziata correttamente, e il valore di ritorno da startTemperature saranno i ms 
  //che bisogna aspettare per ottenere la misura di temperatura.
  // Se invece startTemperature ritorna 0, allora c'è stato un problema all'inizio della misura di temperatura.(collegamento)

  status = pressure.startTemperature();
  if (status != 0)
  {
    
    delay(status); //aspettiamo, che la misura sia ultimata
    status = pressure.getTemperature(T); // salvo nella variabile T il valore della temperatura misurata e ritorno in status 0 se l'operazione di misura, anche se iniziata bene, è fallita, mentre ritorno 1, se 
                                         // se l'operazione di misura è stata ultimata correttamente 
   
    if (status != 0)
    {

      Serial.print("\t\t\t\t");
      Serial.print(T,2);
     
 
      
      // Misura della pressione

      // Iniziamo a misurare la pressione. Se startPressure ritorna un valore diverso da 0, allora la misura è iniziata correttamente, e il valore di ritorno da startPressure saranno i ms 
  //che bisogna aspettare per ottenere la misura di pressione.
  // Se invece startPressure ritorna 0, allora c'è stato un problema all'inizio della misura di pressione.


      status = pressure.startPressure(3); // inizia la lettura di pressione: posso dare un valore come parametro da 0 a 3, più grande è il numero, maggiore sarà la risoluzione della pressioene
                                          // ma ciò comporterà tempi di misura maggiori
      if (status != 0)
      {
        //Aspettiamo che la misura sia ultimata
        delay(status);

        // la misura di pressione viene ultimata

        status = pressure.getPressure(P,T);  // la pressione sarà salvata nella variabileP e c'è bisogno anche della precedente misura di temperatura
                                              // ritorno in status 0 se l'operazione di misura, anche se iniziata bene, è fallita, mentre ritorno 1, se 
                                               // se l'operazione di misura è stata ultimata correttamente 
        if (status != 0)
        {
          
         Serial.print("\t\t\t  ");
          Serial.print(P*100,2);// per 100 per la conversione in Pa
       
         
      


          // determiniamo l'altitudine dal livello del mare
          //Passiamo come parametri la pressione in mB al livello del mare, e la pressione locale misurata in Mb
          //Otteniamo l'altitudine in metri
          a = pressure.altitude(P,PRESSIONE_MARE);
          Serial.print("\t\t");
          Serial.println(a,0);
         
          
        
        }
        else Serial.println("Errore nella misura della pressione\n");
      }
      else Serial.println("Errore iniziale nella misura di pressione\n");
    }
    else Serial.println("Errore nella misura di temperatura\n");
  }
  else Serial.println("Errore nella misura iniziale di temperatura\n");

  delay(3000);  // pausa di 3 sec  
 }
