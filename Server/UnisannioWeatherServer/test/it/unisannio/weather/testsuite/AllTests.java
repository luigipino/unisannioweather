package it.unisannio.weather.testsuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import it.unisannio.weather.db.CityDAOTest;
import it.unisannio.weather.db.CountryDAOTest;
import it.unisannio.weather.db.DAOManagerTest;
import it.unisannio.weather.db.RegionDAOTest;
import it.unisannio.weather.db.WeatherStateDAOTest;
import it.unisannio.weather.domain.measures.WeatherStateTest;
import it.unisannio.weather.sensors.BMP180Test;
import it.unisannio.weather.sensors.DHT11Test;
import it.unisannio.weather.sensors.LDR_VT900_N3Test;
import it.unisannio.weather.sensors.USB_SnifferTest;
import it.unisannio.weather.utils.TimeStampTestCase;
import it.unisannio.weather.utils.WeatherAlgorithmsTest;

	@RunWith(Suite.class)
	@SuiteClasses({CityDAOTest.class, CountryDAOTest.class, DAOManagerTest.class, RegionDAOTest.class ,WeatherStateDAOTest.class , WeatherAlgorithmsTest.class, TimeStampTestCase.class, WeatherStateTest.class, DHT11Test.class, LDR_VT900_N3Test.class, BMP180Test.class, USB_SnifferTest.class})
	public class AllTests {

	}
