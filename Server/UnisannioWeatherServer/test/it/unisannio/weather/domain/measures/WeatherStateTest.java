package it.unisannio.weather.domain.measures;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.OutOfRangeException;
import it.unisannio.weather.utils.TimeStamp;

public class WeatherStateTest {
	
	public static final int MAX_PRESSURE = 110000;
	public static final int MAX_TEMPERATURE = 40; 
	public static final int MAX_HUMIDITY = 90;
	public static final int MAX_LIGHT = 100;
	
	@Test
	public void ConstructorWeatherStateTest() {
		String cityName = "Benevento";
		TimeStamp time = new TimeStamp(Calendar.getInstance());
		WeatherState wt = new WeatherState(cityName,time);
		assertEquals(cityName, wt.getCityName());
		assertEquals(time, wt.getTime());
		assertEquals(0.0, wt.getRainOdds(),0.1);
		assertEquals(0, wt.getMeasures().size());
	}

	@Test
	public void ConstructorEmptyWeatherStateTest() {
		WeatherState wt = new WeatherState();
		assertEquals(0.0, wt.getRainOdds(),0.1);
		assertEquals(0, wt.getMeasures().size());
	}
	
	@Test
	public void setRainOddsTest() {
		WeatherState wt = new WeatherState();
		try {
			wt.setRainOdds(0.2);
			assertEquals(0.2, wt.getRainOdds(),0.1);
		} catch (OutOfRangeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void setRainOddsFailureTest() {
		WeatherState wt = new WeatherState();
		try {
			wt.setRainOdds(3);

		} catch (OutOfRangeException e) {
			// TODO Auto-generated catch block
			assertEquals(0.0, wt.getRainOdds(),0.1);
		}
	}
	
	@Test
	public void setMeasureTest() {
		WeatherState wt = new WeatherState();
		wt.setMeasures(createStubMeasure());
		List<Measure> measures = wt.getMeasures();
		assertEquals(3, measures.size());
		assertNotNull(wt.getRainOdds());
	}
	
	public List<Measure> createStubMeasure(){
		BMP180_Measure bmp = new BMP180_Measure();
		LDR_VT900_N3_Measure ldr = new LDR_VT900_N3_Measure();
		DHT11_Measure dht = new DHT11_Measure();
		Random random = new Random();
		try {
			bmp.addAttribute(new PressureAttribute(random.nextDouble()*MAX_PRESSURE));
			bmp.addAttribute(new TemperatureAttribute(random.nextDouble()*MAX_TEMPERATURE));
			bmp.addAttribute(new AltitudeAttribute(100));
			dht.addAttribute(new HumidityAttribute(random.nextInt(MAX_HUMIDITY)+1));
			ldr.addAttribute(new LightAttribute(random.nextDouble()*MAX_LIGHT));
		} catch (InvalidAttributeException e) {
			//sono misure che generiamo noi quindi non saranno mai errate
		}
		List<Measure> ms = new ArrayList<Measure>();
		ms.add(ldr);
		ms.add(dht);
		ms.add(bmp);
		return ms;
	}
}
