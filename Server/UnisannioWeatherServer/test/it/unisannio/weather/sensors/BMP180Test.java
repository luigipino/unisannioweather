package it.unisannio.weather.sensors;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.Attribute.AttributeName;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;

public class BMP180Test {

	private BMP180 bmp;
	private ArduinoStub stub;

	@Before
	public void setUp() throws FileNotFoundException{
		bmp = new BMP180();
		stub = new ArduinoStub(MeasureName.BMP180, bmp);
	}

	@Test
	public void readTest() throws FileNotFoundException, InvalidAttributeException {
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = bmp.read(stub.getInput());
		} catch (SensorsIOException e) {
			fail("Error reading sensor");
		}
		assertNotNull(m);
		for(Attribute a : m.getAttributes()){
			if(a.getName()==AttributeName.TEMPERATURE)
				assertTrue(a.getValue()>-374);
			else if(a.getName()==AttributeName.PRESSURE)
				assertTrue(a.getValue()>0);
			else
				assertTrue(a.getValue()>-500);
		}
	}

	@Test
	public void readErrorTest() throws FileNotFoundException, InvalidAttributeException {
		stub.setInputFile("bmp180_error_stub.txt");
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = bmp.read(stub.getInput());
		} catch (SensorsIOException e) {
			assertNull(m);
		}
	}

}
