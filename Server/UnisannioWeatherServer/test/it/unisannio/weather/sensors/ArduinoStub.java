package it.unisannio.weather.sensors;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TooManyListenersException;

import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import it.unisannio.weather.domain.measures.Measure.MeasureName;

public class ArduinoStub implements Runnable{
	
	private Sensor sensor;
	private InputStream input;
	
	public ArduinoStub(MeasureName measure, Sensor sensor) throws FileNotFoundException{
		this.sensor= sensor;
		if(measure==MeasureName.BMP180){
			input = new FileInputStream(new File("bmp180_stub.txt"));
		}
		else if(measure==MeasureName.DHT11){
			input = new FileInputStream(new File("dht11_stub.txt"));
		}
		else{
			input = new FileInputStream(new File("ldr_vt900_n3_stub.txt"));
		}
	}

	public void run(){
		SerialPortEvent event = new SerialPortEvent(createStubSerialPort(), SerialPortEvent.DATA_AVAILABLE, false, true);
		try {
			while(input.available()>0)
				sensor.serialEvent(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public InputStream getInput() {
		return input;
	}	
	
	public void setInputFile(String path) throws FileNotFoundException{
		input = new FileInputStream(new File(path));
	}
	
	private SerialPort createStubSerialPort(){
		return new SerialPort() {
			
			@Override
			public void setOutputBufferSize(int arg0) {				
			}
			
			@Override
			public void setInputBufferSize(int arg0) {
			}
			
			@Override
			public boolean isReceiveTimeoutEnabled() {
				return false;
			}
			
			@Override
			public boolean isReceiveThresholdEnabled() {
				return false;
			}
			
			@Override
			public boolean isReceiveFramingEnabled() {
				return false;
			}
			
			@Override
			public int getReceiveTimeout() {
				return 0;
			}
			
			@Override
			public int getReceiveThreshold() {
				return 0;
			}
			
			@Override
			public int getReceiveFramingByte() {
				return 0;
			}
			
			@Override
			public OutputStream getOutputStream() throws IOException {
				return null;
			}
			
			@Override
			public int getOutputBufferSize() {
				return 0;
			}
			
			@Override
			public InputStream getInputStream() throws IOException {				
				return null;
			}
			
			@Override
			public int getInputBufferSize() {
				return 0;
			}
			
			@Override
			public void enableReceiveTimeout(int arg0) throws UnsupportedCommOperationException {
			}
			
			@Override
			public void enableReceiveThreshold(int arg0) throws UnsupportedCommOperationException {
			}
			
			@Override
			public void enableReceiveFraming(int arg0) throws UnsupportedCommOperationException {
			}
			
			@Override
			public void disableReceiveTimeout() {
			}
			
			@Override
			public void disableReceiveThreshold() {
			}
			
			@Override
			public void disableReceiveFraming() {
			}
			
			@Override
			public boolean setUARTType(String arg0, boolean arg1) throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public void setSerialPortParams(int arg0, int arg1, int arg2, int arg3) throws UnsupportedCommOperationException {				
			}
			
			@Override
			public void setRTS(boolean arg0) {
			}
			
			@Override
			public boolean setParityErrorChar(byte arg0) throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public boolean setLowLatency() throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public void setFlowControlMode(int arg0) throws UnsupportedCommOperationException {
			}
			
			@Override
			public boolean setEndOfInputChar(byte arg0) throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public boolean setDivisor(int arg0) throws UnsupportedCommOperationException, IOException {
				return false;
			}
			
			@Override
			public void setDTR(boolean arg0) {
			}
			
			@Override
			public boolean setCallOutHangup(boolean arg0) throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public boolean setBaudBase(int arg0) throws UnsupportedCommOperationException, IOException {
				return false;
			}
			
			@Override
			public void sendBreak(int arg0) {		
			}
			
			@Override
			public void removeEventListener() {	
			}
			
			@Override
			public void notifyOnRingIndicator(boolean arg0) {	
			}
			
			@Override
			public void notifyOnParityError(boolean arg0) {	
			}
			
			@Override
			public void notifyOnOverrunError(boolean arg0) {	
			}
			
			@Override
			public void notifyOnOutputEmpty(boolean arg0) {		
			}
			
			@Override
			public void notifyOnFramingError(boolean arg0) {	
			}
			
			@Override
			public void notifyOnDataAvailable(boolean arg0) {	
			}
			
			@Override
			public void notifyOnDSR(boolean arg0) {	
			}
			
			@Override
			public void notifyOnCarrierDetect(boolean arg0) {
			}
			
			@Override
			public void notifyOnCTS(boolean arg0) {
			}
			
			@Override
			public void notifyOnBreakInterrupt(boolean arg0) {		
			}
			
			@Override
			public boolean isRTS() {
				return false;
			}
			
			@Override
			public boolean isRI() {
				return false;
			}
			
			@Override
			public boolean isDTR() {
				return false;
			}
			
			@Override
			public boolean isDSR() {
				return false;
			}
			
			@Override
			public boolean isCTS() {
				return false;
			}
			
			@Override
			public boolean isCD() {
				return false;
			}
			
			@Override
			public String getUARTType() throws UnsupportedCommOperationException {
				return null;
			}
			
			@Override
			public int getStopBits() {
				return 0;
			}
			
			@Override
			public byte getParityErrorChar() throws UnsupportedCommOperationException {
				return 0;
			}
			
			@Override
			public int getParity() {
				return 0;
			}
			
			@Override
			public boolean getLowLatency() throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public int getFlowControlMode() {
				return 0;
			}
			
			@Override
			public byte getEndOfInputChar() throws UnsupportedCommOperationException {
				return 0;
			}
			
			@Override
			public int getDivisor() throws UnsupportedCommOperationException, IOException {
				return 0;
			}
			
			@Override
			public int getDataBits() {
				return 0;
			}
			
			@Override
			public boolean getCallOutHangup() throws UnsupportedCommOperationException {
				return false;
			}
			
			@Override
			public int getBaudRate() {
				return 0;
			}
			
			@Override
			public int getBaudBase() throws UnsupportedCommOperationException, IOException {
				return 0;
			}
			
			@Override
			public void addEventListener(SerialPortEventListener arg0) throws TooManyListenersException {
			}
		};
	}
}
