package it.unisannio.weather.sensors;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.exceptions.SensorsIOException;


public class USB_SnifferTest {

	private USB_Sniffer sniffer;
	
	@Before
	public void setUp() throws Exception {
		sniffer	= new USB_Sniffer();
	}

	@Test
	public void collectDataTest() throws FileNotFoundException {
		ArduinoStub stub;
		Sensor currentSensor;
		Thread t;
		for(MeasureName measure : MeasureName.values()){
			currentSensor = SensorsHandler.getSensorByMeasure(measure);
			stub = new ArduinoStub(measure, currentSensor);
			sniffer.setInput(stub.getInput());
			t = new Thread(stub);
			t.start();
			try {
				sniffer.collectData(currentSensor);
			} catch (SensorsIOException e) {
				fail("Error in reading sensors");
			}
		}
	}

}
