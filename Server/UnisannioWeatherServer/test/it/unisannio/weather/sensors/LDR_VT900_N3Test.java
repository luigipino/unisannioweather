package it.unisannio.weather.sensors;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;

public class LDR_VT900_N3Test {

	private LDR_VT900_N3 ldr;
	private ArduinoStub stub;
	
	@Before
	public void setUp() throws FileNotFoundException{
		ldr = new LDR_VT900_N3();
		stub = new ArduinoStub(MeasureName.LDR_VT900_N3, ldr);
	}
	
	@Test
	public void readTest() throws FileNotFoundException, InvalidAttributeException {
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = ldr.read(stub.getInput());
		} catch (SensorsIOException e) {
			fail("Error reading sensor");
		}
		assertNotNull(m);
		Attribute a = m.getAttributes().get(0);
		assertTrue(a.getValue()>0);
	}

}
