package it.unisannio.weather.sensors;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;

public class DHT11Test {
	
	private DHT11 dht;
	private ArduinoStub stub;
	
	@Before
	public void setUp() throws FileNotFoundException{
		dht = new DHT11();
		stub = new ArduinoStub(MeasureName.DHT11, dht);
	}
	
	@Test
	public void readTest() throws FileNotFoundException, InvalidAttributeException {
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = dht.read(stub.getInput());
		} catch (SensorsIOException e) {
			fail("Error reading sensor");
		}
		Attribute a = m.getAttributes().get(0);
		assertTrue(a.getValue()>0);
	}

	@Test
	public void readError1Test() throws FileNotFoundException, InvalidAttributeException {
		stub.setInputFile("dht11_error1_stub.txt");
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = dht.read(stub.getInput());
		} catch (SensorsIOException e) {
			assertNull(m);
		}
	}
	
	@Test
	public void readError2Test() throws FileNotFoundException, InvalidAttributeException {
		stub.setInputFile("dht11_error2_stub.txt");
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = dht.read(stub.getInput());
		} catch (SensorsIOException e) {
			assertNull(m);
		}
	}
	
	@Test
	public void readError3Test() throws FileNotFoundException, InvalidAttributeException {
		stub.setInputFile("dht11_error3_stub.txt");
		Thread t = new Thread(stub);
		t.start();
		Measure m = null;
		try {
			m = dht.read(stub.getInput());
		} catch (SensorsIOException e) {
			assertNull(m);
		}
	}
}
