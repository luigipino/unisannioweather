package it.unisannio.weather.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class WeatherAlgorithmsTest {

	@Test
	public void calculateRainOddsTest1() {
		double temp = 28;
		int humidity = 90; 
		assertEquals(0.96,WeatherAlgorithms.calculateRainOdds(temp, humidity),0.1);

	}
	@Test
	public void calculateRainOddsTest2() {
		double temp = 40;
		int humidity = 10; 
		assertEquals(0.0,WeatherAlgorithms.calculateRainOdds(temp, humidity),0.1);

	}

}