package it.unisannio.weather.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.Calendar;

import org.junit.Test;

import it.unisannio.weather.exceptions.TimeFormatException;

public class TimeStampTestCase {

	@Test
	public void TimeStampTest() {
		Calendar calendar = Calendar.getInstance();
		TimeStamp time = new TimeStamp(calendar);
		assertEquals(calendar.get(Calendar.DAY_OF_MONTH), time.getDayOfMonth());
		assertEquals(calendar.get(Calendar.MONTH)+1, time.getMonth());
		assertEquals(calendar.get(Calendar.YEAR), time.getYear());
		assertEquals(calendar.get(Calendar.HOUR_OF_DAY), time.getHoursOfDay());
		assertEquals(calendar.get(Calendar.MINUTE), time.getMinutes());
	}

	@Test
	public void getTimeStampFromStringTest() {
		TimeStamp time;
		try {
			time = TimeStamp.getTimeStampFromString("10-12-2015 10:03");
			assertEquals(10, time.getDayOfMonth());
			assertEquals(12, time.getMonth());
			assertEquals(2015, time.getYear());
			assertEquals(10, time.getHoursOfDay());
			assertEquals(3, time.getMinutes());
		} catch (TimeFormatException e) {
			fail("wrong TimeStamp pattern");
		}
	}
	
	@Test
	public void getTimeStampFromStringFailureWrongDayTest() {
		TimeStamp time = null;
		try {
			time = TimeStamp.getTimeStampFromString("30-02-2015 10:03");
		} catch (TimeFormatException e) {
			assertNull(time);
		}
	}
	
	@Test
	public void getTimeStampFromStringFailureTest() {
		TimeStamp time = null;
		try {
			time = TimeStamp.getTimeStampFromString("10 12-2015 10:03");
			assertEquals(10, time.getDayOfMonth());
			assertEquals(12, time.getMonth());
			assertEquals(2015, time.getYear());
			assertEquals(10, time.getHoursOfDay());
			assertEquals(3, time.getMinutes());
		} catch (TimeFormatException e) {
			assertNull(time);
		}
	}
	
	@Test
	public void toStringTest() {
		String time = "10-12-2015 10:03";
		TimeStamp timeStamp;
		try {
			timeStamp = TimeStamp.getTimeStampFromString(time);
			assertEquals(time, timeStamp.toString());
		} catch (TimeFormatException e) {
			fail("wrong TimeStamp pattern");
		}
	}
	
}
