package it.unisannio.weather.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import it.unisannio.weather.domain.places.City;
import it.unisannio.weather.exceptions.NegativeAltitudeException;

public class CityDAOTest {

	private String url,password,username;
	private Connection connection;
	private City city;
	
	@Before
	public void setUp() throws SQLException, NegativeAltitudeException{
		url = "jdbc:mysql://localhost:3306/unisannio_weather";
		username = "root";
		password = "password";
		connection = (Connection) DriverManager.getConnection(url,username,password);
		city = new City("Napoli","Campania",4051.46);
		city.setAltitude(10);
	}
	
	@Test
	public void testCityDAOConstructor() {
		CityDAO cityDAO = new CityDAO(connection);
		assertNotNull(cityDAO);
	}
	
	@Test
	public void insertGetDeleteTest(){
		CityDAO cityDAO = new CityDAO(connection);
		try {
			cityDAO.insert(city);
			assertEquals(city.getName(), cityDAO.getByKey(city.getName()).getName());
			cityDAO.delete(city.getName());
			assertNull(cityDAO.getByKey(city.getName()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void selectAllTest(){
		CityDAO cityDAO = new CityDAO(connection);
		List<City> cities;
		try {
			cities = cityDAO.selectAll();
			assertNotNull(cities);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateTest(){
		CityDAO cityDAO = new CityDAO(connection);
		try {
			cityDAO.insert(city);
			city.setLatitude(20.0);
			cityDAO.update(city);
			assertEquals(city.getLatitude(), cityDAO.getByKey(city.getName()).getLatitude(),0.0);
			cityDAO.delete(city.getName());
			city.setLatitude(4051.46);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
