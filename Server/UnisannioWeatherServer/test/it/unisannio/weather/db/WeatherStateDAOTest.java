package it.unisannio.weather.db;

import static org.junit.Assert.*;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import it.unisannio.weather.domain.measures.AltitudeAttribute;
import it.unisannio.weather.domain.measures.BMP180_Measure;
import it.unisannio.weather.domain.measures.DHT11_Measure;
import it.unisannio.weather.domain.measures.HumidityAttribute;
import it.unisannio.weather.domain.measures.LDR_VT900_N3_Measure;
import it.unisannio.weather.domain.measures.LightAttribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.PressureAttribute;
import it.unisannio.weather.domain.measures.TemperatureAttribute;
import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.NegativeAltitudeException;
import it.unisannio.weather.utils.TimeStamp;

public class WeatherStateDAOTest {

	public static final int MAX_PRESSURE = 110000;
	public static final int MAX_TEMPERATURE = 40; 
	public static final int MAX_HUMIDITY = 90;
	public static final int MAX_LIGHT = 100;
	private String url,password,username,cityName;
	private Connection connection;
	private WeatherState wstate;
	private TimeStamp time;
	
	@Before
	public void setUp() throws SQLException, NegativeAltitudeException{
		url = "jdbc:mysql://localhost:3306/unisannio_weather";
		username = "root";
		password = "password";
		connection = (Connection) DriverManager.getConnection(url,username,password);
		cityName = "Benevento";
		time = new TimeStamp(Calendar.getInstance());
		wstate = new WeatherState(cityName,time);
		wstate.setMeasures(createStubMeasure());
	}
	
	@Test
	public void constructorTest(){
		WeatherStateDAO wstateDAO = new WeatherStateDAO(connection);
		assertNotNull(wstateDAO);
	}
	
	@Test
	public void insertGetDeleteTest(){
		WeatherStateDAO wstateDAO = new WeatherStateDAO(connection);
		try {
			wstateDAO.insert(wstate);
			assertEquals(wstate.getCityName(), wstateDAO.getByKey("Benevento",time.toString()).getCityName());
			wstateDAO.delete(cityName,time.toString());
			assertNull(wstateDAO.getByKey("Benevento",time.toString()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getLastWeatherStateByCityTest(){
		WeatherStateDAO wstateDAO = new WeatherStateDAO(connection);
		try {
			assertNotNull(wstateDAO.getLastWeatherStateByCity(cityName));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getAllWeatherStateByCityTest(){
		WeatherStateDAO wstateDAO = new WeatherStateDAO(connection);
		try {
			assertNotNull(wstateDAO.getAllWeatherStateByCity(cityName));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Measure> createStubMeasure(){
		BMP180_Measure bmp = new BMP180_Measure();
		LDR_VT900_N3_Measure ldr = new LDR_VT900_N3_Measure();
		DHT11_Measure dht = new DHT11_Measure();
		Random random = new Random();
		try {
			bmp.addAttribute(new PressureAttribute(random.nextDouble()*MAX_PRESSURE));
			bmp.addAttribute(new TemperatureAttribute(random.nextDouble()*MAX_TEMPERATURE));
			bmp.addAttribute(new AltitudeAttribute(100));
			dht.addAttribute(new HumidityAttribute(random.nextInt(MAX_HUMIDITY)+1));
			ldr.addAttribute(new LightAttribute(random.nextDouble()*MAX_LIGHT));
		} catch (InvalidAttributeException e) {
			//sono misure che generiamo noi quindi non saranno mai errate
		}
		List<Measure> ms = new ArrayList<Measure>();
		ms.add(ldr);
		ms.add(dht);
		ms.add(bmp);
		return ms;
	}
}
