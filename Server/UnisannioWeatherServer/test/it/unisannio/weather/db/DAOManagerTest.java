package it.unisannio.weather.db;

import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import it.unisannio.weather.exceptions.DBConnectionException;
import it.unisannio.weather.exceptions.NegativeAltitudeException;

public class DAOManagerTest {

	private String url,password,username;
	
	
	@Before
	public void setUp() throws SQLException, NegativeAltitudeException{
		url = "jdbc:mysql://localhost:3306/unisannio_weather";
		username = "root";
		password = "password";
		
	}
	
	@Test
	public void constructorTest() {
		try {
			assertNotNull(DAOManager.getInstance(url, username, password));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getCityDAO(){
		try {
			DAOManager daoManager = DAOManager.getInstance(url, username, password);
			CityDAO cityDAO = daoManager.getCityDAO();
			assertNotNull(cityDAO);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getRegionDAO(){
		try {
			DAOManager daoManager = DAOManager.getInstance(url, username, password);
			RegionDAO regionDAO = daoManager.getRegionDAO();
			assertNotNull(regionDAO);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getCountryDAO(){
		try {
			DAOManager daoManager = DAOManager.getInstance(url, username, password);
			CountryDAO countryDAO = daoManager.getCountryDAO();
			assertNotNull(countryDAO);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getWeatherStateDAO(){
		try {
			DAOManager daoManager = DAOManager.getInstance(url, username, password);
			WeatherStateDAO weatherStateDAO = daoManager.getWeatherStateDAO();
			assertNotNull(weatherStateDAO);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DBConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
