package it.unisannio.weather.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import it.unisannio.weather.domain.places.Region;
import it.unisannio.weather.exceptions.NegativeAltitudeException;

public class RegionDAOTest {

	private String url,password,username;
	private Connection connection;
	private Region region;
	
	@Before
	public void setUp() throws SQLException, NegativeAltitudeException{
		url = "jdbc:mysql://localhost:3306/unisannio_weather";
		username = "root";
		password = "password";
		connection = (Connection) DriverManager.getConnection(url,username,password);
		region = new Region("Andalusia","ES");
	}
	
	@Test
	public void constructorTest(){
		RegionDAO regionDAO = new RegionDAO(connection);
		assertNotNull(regionDAO);
	}

	@Test
	public void insertGetDeleteTest(){
		RegionDAO regionDAO = new RegionDAO(connection);
		try {
			regionDAO.insert(region);
			assertEquals(region.getName(), regionDAO.getByKey(region.getName(),region.getCountryName()).getName());
			regionDAO.delete(region.getName(),region.getCountryName());
			assertNull(regionDAO.getByKey(region.getName(),region.getCountryName()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void selectAllTest(){
		RegionDAO regionDAO = new RegionDAO(connection);
		Map<String, Region> regions;
		try {
			regions = regionDAO.selectAll();
			assertNotNull(regions);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
