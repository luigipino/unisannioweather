package it.unisannio.weather.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import it.unisannio.weather.domain.places.Country;
import it.unisannio.weather.exceptions.NegativeAltitudeException;

public class CountryDAOTest {

	private String url,password,username;
	private Connection connection;
	private Country country;
	
	@Before
	public void setUp() throws SQLException, NegativeAltitudeException{
		url = "jdbc:mysql://localhost:3306/unisannio_weather";
		username = "root";
		password = "password";
		connection = (Connection) DriverManager.getConnection(url,username,password);
		country = new Country("Polonia","PO");
	}
	
	@Test
	public void testCountryDAOConstructor() {
		CountryDAO countryDAO = new CountryDAO(connection);
		assertNotNull(countryDAO);
	}
	
	@Test
	public void insertGetDeleteTest(){
		CountryDAO countryDAO = new CountryDAO(connection);
		try {
			countryDAO.insert(country);
			assertEquals(country.getName(), countryDAO.getByKey(country.getId()).getName());
			countryDAO.delete(country.getId());
			assertNull(countryDAO.getByKey(country.getName()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void updateTest(){
		CountryDAO countryDAO = new CountryDAO(connection);
		country.setName("Slovacchia");
		try {
			countryDAO.insert(country);
			countryDAO.update(country);
			assertEquals(country.getName(), countryDAO.getByKey(country.getId()).getName());
			countryDAO.delete(country.getId());
			country.setName("Polonia");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
