#include <SFE_BMP180.h> // includiamo la libreria del sensore
#include <Wire.h>  // includiamo la libreria wire per la comunicazioneI2C

SFE_BMP180 pressure; // istanziamo un oggetto

#define PRESSIONE_MARE 1013.25 // pressione sul livello del mare in mb

void setup()
{
  Serial.begin(9600);
}

void loop()
{
 //inizializziamo il sensore(fase di calibrazione)
 if (pressure.begin()){
    Serial.println("BMP180 inizializzato con successo");
  char status;
  double T,P,a;

 Serial.println("start_read");
  
 
  // tale sensore funziona correttamente andando a misurare prima la temperatura e poi la pressione(datasheet)
  // Misura di temperatura
  // Iniziamo a misurare la temperatura. Se startTemperature ritorna un valore diverso da 0, allora la misura è iniziata correttamente, e il valore di ritorno da startTemperature saranno i ms 
  //che bisogna aspettare per ottenere la misura di temperatura.
  // Se invece startTemperature ritorna 0, allora c'è stato un problema all'inizio della misura di temperatura.(collegamento)

  status = pressure.startTemperature();
  if (status != 0)
  {
    
    delay(status); //aspettiamo, che la misura sia ultimata
    status = pressure.getTemperature(T); // salvo nella variabile T il valore della temperatura misurata e ritorno in status 0 se l'operazione di misura, anche se iniziata bene, è fallita, mentre ritorno 1, se 
                                         // se l'operazione di misura è stata ultimata correttamente 
   
    if (status != 0)
    {

      //Serial.print("temperatura: ");
      Serial.println(T,2);
      //Serial.println(" C");
 
      
      // Misura della pressione

      // Iniziamo a misurare la pressione. Se startPressure ritorna un valore diverso da 0, allora la misura è iniziata correttamente, e il valore di ritorno da startPressure saranno i ms 
  //che bisogna aspettare per ottenere la misura di pressione.
  // Se invece startPressure ritorna 0, allora c'è stato un problema all'inizio della misura di pressione.


      status = pressure.startPressure(3); // inizia la lettura di pressione: posso dare un valore come parametro da 0 a 3, più grande è il numero, maggiore sarà la risoluzione della pressioene
                                          // ma ciò comporterà tempi di misura maggiori
      if (status != 0)
      {
        //Aspettiamo che la misura sia ultimata
        delay(status);

        // la misura di pressione viene ultimata

        status = pressure.getPressure(P,T);  // la pressione sarà salvata nella variabileP e c'è bisogno anche della precedente misura di temperatura
                                              // ritorno in status 0 se l'operazione di misura, anche se iniziata bene, è fallita, mentre ritorno 1, se 
                                               // se l'operazione di misura è stata ultimata correttamente 
        if (status != 0)
        {
          
          //Serial.print("Pressione assoluta nel luogo di misura: ");
          Serial.println(P*100,2);// per 100 per la conversione in Pa
          //Serial.println(" Pa");
         
      


          // determiniamo l'altitudine dal livello del mare
          //Passiamo come parametri la pressione in mB al livello del mare, e la pressione locale misurata in Mb
          //Otteniamo l'altitudine in metri
          a = pressure.altitude(P,PRESSIONE_MARE);
          //Serial.print("Altitudine approssimata del luogo di misura: ");
          Serial.println(a,1);
         // Serial.println(" metri");
          
        
        }
        else Serial.println("Errore nella misura della pressione\n");
      }
      else Serial.println("Errore iniziale nella misura di pressione\n");
    }
    else Serial.println("Errore nella misura di temperatura\n");
  }
  else Serial.println("Errore nella misura iniziale di temperatura\n");

  Serial.println("End_read");

 }
  else
  {
    //probabilmente c'è qualche collegamento fatto male e quindi non riesco a inizializzarlo correttamente il sensore
    Serial.println("BMP180 inizializzazione fallita\n\n");
  }
    delay(3000);  // pausa di 3 secondi
}
