#include <dht11.h> //includo la libreria

 
dht11 DHT11 ; // istanzio un oggetto di tale classe

#define DHT11_PIN 4 // definisco la costante del pin digitale dove fare la lettura
int chk =0;     // imposto una variabile di tipo integer (chk) in cui memorizzerai i valori restituiti dall’interrogazione della sonda;
void setup(){
        Serial.begin(9600);
        //Serial.println("Sensore \tStato \tUmidita'(%) \tTemperatura(C)");
}
 
void loop(){
  
        //Serial.print("DHT11\t\t");
        chk = DHT11.read(DHT11_PIN);    // leggo i dati del sensore
        switch (chk){                  // la variabile chk mi indica lo stato del sensore
          case DHTLIB_OK:              // se la variabile vale 0, che è il valore della costante DHTLIB_OK allora il sensore campiona e funziona correttamente             
                      Serial.println("OK"); 
                      break;
          case DHTLIB_ERROR_CHECKSUM: // se la variabile vale -1, che è il valore della costante DHTLIB_ERROR allora il sensore non passa la fase di test e i dati nn sono corretti
                      Serial.println("Errore1"); 
                      break;
          case DHTLIB_ERROR_TIMEOUT: // se la variabile vale -2, che è il valore della costante DHTLIB_ERROR_T allora il sensore non comunica correttamente
                      Serial.println("Errore2"); 
                      break;
          default: 
                      Serial.println("Errore3"); 
                      break;
  }

  Serial.println(DHT11.humidity); // risoluzione sensore 1%
  //Serial.print("\t\t  ");
 // Serial.println(DHT11.temperature);  // risoluzone sensore 1 grado, per questo metto 1 dopo println

 
  delay(4000);
}
