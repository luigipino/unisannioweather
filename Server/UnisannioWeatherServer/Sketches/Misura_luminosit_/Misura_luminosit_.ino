// Misura della luminosità mediante LDR VT9N3
// In futuro dovremmo gestire in Java tutto l'analisi della resistenza e della luminisotià e ridurre all'osso Arduino andando solo a leggere la misura sulla resistenza del partitore
// con l'LDR, poichè è lì che usiamo le funzionalità di arduino
// Bisogna gestire sempre in java il caso in cui il valore di sensorValue è 0, in quanto in questo caso siamo al buio più assoluto e la resistenza dell'LDR è circa 1 MOhm, mentre il software mi dice 
// che la resistenza dell'LDR è INFINITO


#include <math.h>    // libreria per il logaritmo
#define v_In 5.00    // Tensione di ingresso 5 V
#define r_2 10000.00  // Definiamo la resistenza da 10k del partitore  

int analogInPin = A0;  // Definiamo il pin analogico
int sensorValue = 0;  // Definiamo il valore in digitale che viene letto dal pin analogico
double v_BC= 0.00;          // Definiamo la tensione letta sulla resistenza da 10 k presente nel partitore di tensione con l'LDR
double r_LDR = 0.00;      // Inizializziamo la resistenza dell'LDR   
double fc = 0.00;        // Inizializziamo le fotocandele della luminosità



void setup() {
  Serial.begin(9600);            //Inizializzo il monitor seriale a una trasmissione di 9600 baud
  pinMode(analogInPin, INPUT);    // Inizializzo il pin A0 come INPUT
}
 
void loop() {

  sensorValue = analogRead(analogInPin);            // Vado a leggere il valore di A0
 
  v_BC=( ( v_In/1024 )* ((double) sensorValue));              // Converto questo valore letto sensorValue sul pin A0( che è un valore tra 0 e 1023( in quanto si ha una risoluzione a 8 bit)) in Volt
                                                   // dove ogni singolo passo di quantizzazione corrisponde a 4,9 mV (cioè a 5/1024)

//  Serial.print("Tensione resistenza 10 k= " );
  
//  Serial.println(v_BC);
  
  r_LDR = (r_2 * v_In/v_BC )- r_2;         // Applicando il partitore mi ricavo la resistenza dell'LDR attraverso questa espressione
  
 // Serial.print("Resistenza LDR= " );
//  Serial.println(r_LDR);
  
 
  
  fc = pow(10,(log10(r_LDR) - 4.7 )/ (-0.86));  // attraverso questa espressione che riporteremo per intero nell'analisi circuitale dei sensori nella relazione, mi ricavo le fotocandele dalla r_LDR
  
 //  Serial.print("Fotocandele= " );
  Serial.println(fc);
  
  
  delay(5000); // ritardo di 2 secondi tra una lettura e l'altra
}
