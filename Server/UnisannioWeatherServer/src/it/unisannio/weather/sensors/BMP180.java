package it.unisannio.weather.sensors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import gnu.io.SerialPortEvent;
import it.unisannio.weather.domain.measures.AltitudeAttribute;
import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.BMP180_Measure;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.PressureAttribute;
import it.unisannio.weather.domain.measures.TemperatureAttribute;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;
import it.unisannio.weather.utils.Constants;

//A BMP180 sensor is used to read temperature, pressure and altitude
public class BMP180 extends Sensor{

	private double temperature,pressure, altitude;
	private  BufferedReader br;
	private static Logger logger = Logger.getLogger(BMP180.class);

	public BMP180() {
		temperature = -400.0;
		pressure = -2.0;
		altitude = -600.0;
		logger.setLevel(Level.ALL);
	}
	
	@Override
	public void init() throws IOException {
		String sketchPath = Constants.BMP180_SKETCH_PATH;
		String arduinoPath = Constants.ARDUINO_COMMAND_PATH;
		String batchPath = Constants.LOAD_SKETCH_PATH;
		String command = batchPath+" "+arduinoPath+" "+sketchPath;
		Process p = Runtime.getRuntime().exec(command);
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = br.readLine();
		while(line!=null){
			logger.info(line);
			line = br.readLine();
		}

	}

	@Override
	public Measure read(InputStream input) throws InvalidAttributeException, SensorsIOException{
		br = new BufferedReader(new InputStreamReader(input));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			if(temperature<=-374.0 || pressure<=-1.0 || altitude<=-500.0)
				throw new SensorsIOException();
		}
		if(temperature<=-374.0 || pressure<=-1.0 || altitude<=-500.0)
			throw new SensorsIOException();
		Attribute a = new TemperatureAttribute(temperature);
		Attribute b = new PressureAttribute(pressure);
		Attribute c = new AltitudeAttribute(altitude);
		Measure m = new BMP180_Measure();
		m.addAttribute(a);
		m.addAttribute(b);
		m.addAttribute(c);
		return m;
	}

	@Override
	public void serialEvent(SerialPortEvent event) {
		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			String inputLine="";
			try {
				do{
					inputLine = br.readLine();
				}
				while(!inputLine.contains("BMP180 inizializzato con successo"));
				inputLine = br.readLine();
				if(inputLine.contains("start_read")){
					inputLine = br.readLine();
					if(!inputLine.contains("Errore nella misura iniziale di temperatura") || !inputLine.contains("Errore nella misura di temperatura")){
						temperature = Double.parseDouble(inputLine);
						logger.info("Temperature: "+temperature);
						inputLine = br.readLine();
						if(!inputLine.contains("Errore iniziale nella misura di pressione") || !inputLine.contains("Errore nella misura della pressione")){
							pressure = Double.parseDouble(inputLine);
							logger.info("Pressure: "+pressure);
							inputLine = br.readLine();
							altitude = Double.parseDouble(inputLine);
							logger.info("Altitude: "+altitude);
						}
						else{
							//errore pressione
							logger.error("Error in reading pressure");
							temperature = -374;
							pressure = -1;
							altitude = -500;
						}
					}
					else{
						//errore lettura temperatura
						logger.error("Error in reading temperature");
						temperature = -374;
						pressure = -1;
						altitude = -500;
					}
				}
				else{
					//errore sincronizzazione lettura
					logger.error("Error in reading synchronization");
					temperature = -374;
					pressure = -1;
					altitude = -500;
				}

			} catch (IOException e) {
				//non facciamo niente, poich� una lettura sbagliata pu� essere seguita da una corretta
				//in caso di pi� letture sbagliate, il metodo read lancia l'eccezione appropriata
			}
		}
	}

}
