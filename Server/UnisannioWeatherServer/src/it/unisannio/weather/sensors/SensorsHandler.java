package it.unisannio.weather.sensors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TooManyListenersException;

import org.apache.log4j.Logger;

import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;
import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.exceptions.SensorsIOException;
import it.unisannio.weather.utils.TimeStamp;

//A sensor handler manages the sequence of loading a sketch on the Arduino board and collecting the related measures 
public class SensorsHandler {
	
	private USB_Sniffer sniffer;
	private static final Logger logger = Logger.getLogger(SensorsHandler.class);
	private String cityName;
	
	public SensorsHandler(String cityName){
		sniffer = new USB_Sniffer();
		this.cityName = cityName;
	}
	
	public String getCityName(){
		return cityName;
	}
	
	public void setCityName(String cityName){
		this.cityName = cityName;
	}
	
	
	public WeatherState getWeatherState() throws SensorsIOException{
		WeatherState ws = new WeatherState(cityName, new TimeStamp(GregorianCalendar.getInstance()));
		ws.setMeasures(runSniffer());
		return ws;
	}
	
	//This method collects data for each supported sensors
	private List<Measure> runSniffer() throws SensorsIOException{
		List<Measure> measures = new ArrayList<Measure>();
		Sensor currentSensor;
		for(MeasureName name : MeasureName.values()){
			currentSensor = getSensorByMeasure(name);
			Thread t = new Thread(currentSensor);
			t.start();
			try {
				logger.trace("Main thread sleep to initialize Arduino...");
				Thread.sleep(40000);
				logger.trace("Main thread resumed...");
				sniffer.init(currentSensor);
			} catch (PortInUseException e) {
				logger.error("Selected USB port in use");
				throw new SensorsIOException();
			} catch (UnsupportedCommOperationException e) {
				logger.error("Unsupported Comm Operation");
				throw new SensorsIOException();
			} catch (IOException e) {
				logger.error("Error reading sensors");
				throw new SensorsIOException();
			} catch (TooManyListenersException e) {
				logger.error("Too many listener");
				throw new SensorsIOException();
			} catch (InterruptedException e) {
				logger.error("Error reading sensors");
				throw new SensorsIOException();
			}
			measures.add(sniffer.collectData(currentSensor));
			for(Measure m: measures){
				for(Attribute a:m.getAttributes())
					logger.info(a.getName()+": "+a.getValue());
			}
			sniffer.stop();
		}
		
		try {
			//carichiamo sketch vuoto
			sniffer.cleanBoard();
		} catch (IOException e) {
			logger.error("Error in loading empty sketch");
		}
		return measures;
	}
	
	protected static Sensor getSensorByMeasure(MeasureName measureType){
		switch(measureType){
		case BMP180: return new BMP180();
		case DHT11: return new DHT11();
		case LDR_VT900_N3: return new LDR_VT900_N3();
		default: return null;
		}
	}
	
}
