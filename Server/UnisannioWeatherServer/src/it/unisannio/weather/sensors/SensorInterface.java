package it.unisannio.weather.sensors;

import java.io.IOException;
import java.io.InputStream;

import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;

//Interface that includes all sensor methods
public interface SensorInterface {
	
	//This method loads the specific Arduino sketch on the Arduino board
	public void init() throws IOException;
	
	//Read method returns a specific Measure made by an Attribute list 
	public Measure read(InputStream input) throws InvalidAttributeException, SensorsIOException;


}
