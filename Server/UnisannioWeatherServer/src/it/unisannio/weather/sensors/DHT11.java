package it.unisannio.weather.sensors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import gnu.io.SerialPortEvent;
import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.DHT11_Measure;
import it.unisannio.weather.domain.measures.HumidityAttribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;
import it.unisannio.weather.utils.Constants;

//A DHT11 sensor is used to read humidity
public class DHT11 extends Sensor{

	private int humidity;
	private BufferedReader br;
	private static Logger logger = Logger.getLogger(DHT11.class);

	public DHT11() {
		humidity = -20;
		logger.setLevel(Level.ALL);
	}

	@Override
	public void init() throws IOException {
		String sketchPath = Constants.DHT11_SKETCH_PATH;
		String arduinoPath = Constants.ARDUINO_COMMAND_PATH;
		String batchPath = Constants.LOAD_SKETCH_PATH;
		String command = batchPath+" "+arduinoPath+" "+sketchPath;
		Process p = Runtime.getRuntime().exec(command);
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = br.readLine();
		while(line!=null){
			logger.info(line);
			line = br.readLine();
		}
	}

	@Override
	public Measure read(InputStream input) throws InvalidAttributeException, SensorsIOException{
		br = new BufferedReader(new InputStreamReader(input));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {
			if(humidity<=-10)
				throw new SensorsIOException();
		}
		if(humidity<=-10)
			throw new SensorsIOException();
		Attribute a = new HumidityAttribute(humidity);
		Measure m = new DHT11_Measure();
		m.addAttribute(a);
		return m;
	}

	@Override
	public void serialEvent(SerialPortEvent event) {
		//default humidity compresa tra 0 e 100 (noi tra 20 e 90)
		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			String inputLine="";
			try {
				inputLine = br.readLine();
				if(inputLine.contains("OK")){
					inputLine = br.readLine();
					humidity = Integer.parseInt(inputLine);
					logger.info("Humidity: "+humidity);
				}
				else if(inputLine.contains("Errore1")){
					//Errore nei dati
					logger.error("Error in reading data");
					humidity=-1;
				}
				else if(inputLine.contains("Errore2")){
					//Errore nella comunicazione
					logger.error("Error in communication");
					humidity=-2;
				}
				else if(inputLine.contains("Errore3")){
					//Errore sconosciuto
					logger.error("Unknown error");
					humidity=-3;
				}
			} catch (IOException e) {
				//non facciamo niente, poich� una lettura sbagliata pu� essere seguita da una corretta
				//in caso di pi� letture sbagliate, il metodo read lancia l'eccezione appropriata
			}
			try{
				humidity = Integer.parseInt(inputLine);
			}
			catch(NumberFormatException e){
				humidity = -10;
			}
		}
	}

}
