package it.unisannio.weather.sensors;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import it.unisannio.weather.db.DAOManager;
import it.unisannio.weather.db.WeatherStateDAO;
import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.Attribute.AttributeName;
import it.unisannio.weather.domain.measures.BMP180_Measure;
import it.unisannio.weather.domain.measures.DHT11_Measure;
import it.unisannio.weather.domain.measures.HumidityAttribute;
import it.unisannio.weather.domain.measures.LDR_VT900_N3_Measure;
import it.unisannio.weather.domain.measures.LightAttribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.domain.measures.PressureAttribute;
import it.unisannio.weather.domain.measures.TemperatureAttribute;
import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.domain.places.City;
import it.unisannio.weather.exceptions.DBConnectionException;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.NegativeAltitudeException;
import it.unisannio.weather.exceptions.SensorsIOException;
import it.unisannio.weather.utils.Constants;
import it.unisannio.weather.utils.TimeStamp;

//Sensor Manager stores Benevento real data in the database and loads stub data for other cities
public class SensorsManager extends TimerTask{
	public static TimeStamp currentTime;
	public static final int MAX_PRESSURE = 110000;
	public static final int MAX_TEMPERATURE = 40; 
	public static final int MAX_HUMIDITY = 90;
	public static final int MAX_LIGHT = 100;
	public static final int DB_CONNECTION_ERROR = -1; 
	public static final int SQL_ERROR = -2; 
	public static final int SENSORS_ERROR = -3; 
	public static final int STUB_FILE_READING_ERROR = -4;
	public static final int CLASS_DRIVER_ERROR = -5;
	public static final int OK = 1;
	private static int statusCode;
	private static List<String> supportedCities;

	public synchronized static int getStatusCode(){
		return statusCode;
	}

	//stores the whaterState in the database
	private static void storeWeatherState(WeatherState state) throws DBConnectionException, SQLException, ClassNotFoundException{
		double altitude = 0;
		WeatherStateDAO wsd = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getWeatherStateDAO();
		wsd.insert(state);
		for(Measure m : state.getMeasures()){
			if(m.getName()==MeasureName.BMP180){
				List<Attribute> attr = m.getAttributes();
				for(Attribute a: attr){
					if(a.getName()==AttributeName.ALTITUDE)
						altitude = a.getValue();
					else continue;
				}
			}
			else continue;
		}
		City c = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getCityDAO().getByKey(state.getCityName());
		try {
			if(c.getAltitude()==0){
				c.setAltitude(altitude);
				DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getCityDAO().update(c);
			}
		} catch (NegativeAltitudeException e) {
			//niente
		}
	}

	//reads the supported stub cities 
	private static List<String> getSupportedCities() throws IOException{
		List<String> cities = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("SupportedCities.txt")));
		String city = br.readLine();
		while(city!=null){
			cities.add(city);
			city = br.readLine();
		}		
		br.close();
		return cities;
	}

	private static void loadStubData(List<String> stubCities) throws DBConnectionException, SQLException, ClassNotFoundException{
		WeatherState wState;
		for(String city: stubCities){
			wState = generateStubWeatherState(city);
			storeWeatherState(wState);
			System.out.println("Stored stub data for: "+city+ " at time: "+Calendar.getInstance().getTime().toString());
		}
	}

	private static WeatherState generateStubWeatherState(String cityName){
		WeatherState ws = new WeatherState(cityName, currentTime);
		BMP180_Measure bmp = new BMP180_Measure();
		LDR_VT900_N3_Measure ldr = new LDR_VT900_N3_Measure();
		DHT11_Measure dht = new DHT11_Measure();
		Random random = new Random();
		try {
			bmp.addAttribute(new PressureAttribute(random.nextDouble()*MAX_PRESSURE));
			bmp.addAttribute(new TemperatureAttribute(random.nextDouble()*MAX_TEMPERATURE));
			dht.addAttribute(new HumidityAttribute(random.nextInt(MAX_HUMIDITY)+1));
			ldr.addAttribute(new LightAttribute(random.nextDouble()*MAX_LIGHT));
		} catch (InvalidAttributeException e) {
			//sono misure che generiamo noi quindi non saranno mai errate
		}
		List<Measure> ms = new ArrayList<Measure>();
		ms.add(ldr);
		ms.add(dht);
		ms.add(bmp);
		ws.setMeasures(ms);
		return ws;
	}


	public static void main(String[] args) throws IOException {
		supportedCities = getSupportedCities();
		for(String s: supportedCities)
			System.out.println("City: "+s);
		Timer timer = new Timer();
		Calendar date = Calendar.getInstance();
		date.set(Calendar.MINUTE, 60);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		timer.schedule(new SensorsManager(), date.getTime(), TimeUnit.MILLISECONDS.convert(1L, TimeUnit.HOURS));		
	}

	@Override
	public void run() {
		try {
			DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD);
			currentTime = new TimeStamp(GregorianCalendar.getInstance());
			SensorsHandler sh = new SensorsHandler("Benevento");
			storeWeatherState(sh.getWeatherState());
			loadStubData(supportedCities);
			DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).destroy();
		} catch (DBConnectionException e) {
			statusCode = DB_CONNECTION_ERROR;
			System.out.println(statusCode);
			return; //TODO da cambiare in skip con il timer
		} catch (SQLException e) {
			statusCode = SQL_ERROR;
			System.out.println(statusCode);
			e.printStackTrace();
			return;
			//		} catch (SensorsIOException e) {
			//			statusCode = SENSORS_ERROR;
			//			return;
//		} catch (IOException e) {
//			statusCode = STUB_FILE_READING_ERROR;
//			System.out.println(statusCode);
//			return;
		} catch (ClassNotFoundException e) {
			statusCode = CLASS_DRIVER_ERROR;
			System.out.println(statusCode);
			return;
		} catch (SensorsIOException e) {
			statusCode = SENSORS_ERROR;
			System.out.println(statusCode);
			return;
		}
		statusCode = OK;
	}

}
