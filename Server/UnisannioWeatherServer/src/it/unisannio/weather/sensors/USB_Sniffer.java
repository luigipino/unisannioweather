package it.unisannio.weather.sensors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import org.apache.log4j.Logger;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;
import it.unisannio.weather.utils.Constants;

//USB sniffer handles the data sent by the Arduino board by listening on the USB port 
public class USB_Sniffer {

	private SerialPort serialPort;
	/** The port we're normally going to use. */
	private static final String PORT_NAMES[] = { 
			"/dev/tty.usbserial-A9007UX1", // Mac OS X
			"/dev/ttyACM0", // Raspberry Pi
			"/dev/ttyUSB0", // Linux
			"COM4", // Windows
	};
	/**
	 * A BufferedReader which will be fed by a InputStreamReader 
	 * converting the bytes into characters 
	 * making the displayed results codepage independent
	 */
	private InputStream input;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	private static final Logger logger = Logger.getLogger(USB_Sniffer.class);
	
	public USB_Sniffer(){
		
	}
	
	//The sniffer starts to listen on the specified USB port
	@SuppressWarnings("rawtypes")
	public void init(Sensor sensor) throws PortInUseException, UnsupportedCommOperationException, IOException, TooManyListenersException{
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			for (String portName : PORT_NAMES) {
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			}
		}
		if (portId == null) {
			throw new PortInUseException();
		}
		// open serial port, and use class name for the appName.
		serialPort = (SerialPort) portId.open(this.getClass().getName(),
				TIME_OUT);

		// set port parameters
		serialPort.setSerialPortParams(DATA_RATE,
				SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1,
				SerialPort.PARITY_NONE);

		// open the streams
		input = serialPort.getInputStream();

		// add event listeners
		serialPort.addEventListener(sensor);
		serialPort.notifyOnDataAvailable(true);
	}

	//Read the data of a specified sensor
	public Measure collectData(SensorInterface sensor) throws SensorsIOException{
		if(sensor == null) throw new SensorsIOException();
		Measure measure = null;
		try {
			measure = sensor.read(input);
		} catch (InvalidAttributeException e) {
		}
		return measure;
	}

	public synchronized void stop() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}
	
	//Loads on the Arduino board the empty sketch
	public void cleanBoard() throws IOException{
		String sketchPath = Constants.EMPTY_SKETCH;
		String arduinoPath = Constants.ARDUINO_COMMAND_PATH;
		String batchPath = Constants.LOAD_SKETCH_PATH;
		String command = batchPath+" "+arduinoPath+" "+sketchPath;
		Process p = Runtime.getRuntime().exec(command);
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = br.readLine();
		while(line!=null){
			logger.info(line);
			line = br.readLine();
		}
	}

	public void setInput(InputStream input) {
		this.input=input;
	}
	
	
}
