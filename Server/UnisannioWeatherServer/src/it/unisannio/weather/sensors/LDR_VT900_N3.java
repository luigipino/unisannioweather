package it.unisannio.weather.sensors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import gnu.io.SerialPortEvent;
import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.LDR_VT900_N3_Measure;
import it.unisannio.weather.domain.measures.LightAttribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.SensorsIOException;
import it.unisannio.weather.utils.Constants;

//A LDR_VT900_N3 sensor is used to read light
public class LDR_VT900_N3 extends Sensor{

	private double light;
	private BufferedReader br;
	private static Logger logger = Logger.getLogger(LDR_VT900_N3.class);

	public LDR_VT900_N3() {
		super();
		this.light = -2.0;
		logger.setLevel(Level.ALL);
	}

	@Override
	public void init() throws IOException {
		String sketchPath = Constants.LDR_SKETCH_PATH;
		String arduinoPath = Constants.ARDUINO_COMMAND_PATH;
		String batchPath = Constants.LOAD_SKETCH_PATH;
		String command = batchPath+" "+arduinoPath+" "+sketchPath;
		Process p = Runtime.getRuntime().exec(command);
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = br.readLine();
		while(line!=null){
			logger.info(line);
			line = br.readLine();
		}
	}

	@Override
	public Measure read(InputStream input) throws InvalidAttributeException, SensorsIOException {
		br = new BufferedReader(new InputStreamReader(input));
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {
			if(light<=-1)
				throw new SensorsIOException();
		}
		if(light<=-1)
			throw new SensorsIOException();
		Attribute a = new LightAttribute(light);
		Measure m = new LDR_VT900_N3_Measure();
		m.addAttribute(a);
		return m;
	}

	@Override
	public synchronized void serialEvent(SerialPortEvent event) {
		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			String inputLine="";
			try {
				inputLine = br.readLine();
			} catch (IOException e) {
				//non facciamo niente, poich� una lettura sbagliata pu� essere seguita da una corretta
				//in caso di pi� letture sbagliate, il metodo read lancia l'eccezione appropriata
			}
			try{
				light = Double.parseDouble(inputLine);
			}
			catch(NumberFormatException e){
				light = -1.0;
			}
			logger.info("Light: "+light);
		}

	}

}
