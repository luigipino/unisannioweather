package it.unisannio.weather.exceptions;

public class TimeFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TimeFormatException() {
		super();
	}

	public TimeFormatException(String message) {
		super(message);
	}
	
}
