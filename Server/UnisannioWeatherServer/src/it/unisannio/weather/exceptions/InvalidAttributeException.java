package it.unisannio.weather.exceptions;

public class InvalidAttributeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidAttributeException() {
		super();
	}

	public InvalidAttributeException(String message) {
		super(message);
	}
	
	

}
