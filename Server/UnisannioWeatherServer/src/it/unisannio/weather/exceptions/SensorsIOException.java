package it.unisannio.weather.exceptions;

public class SensorsIOException extends Exception{

	private static final long serialVersionUID = 1L;

	public SensorsIOException() {
		super();
	}

	public SensorsIOException(String message) {
		super(message);
	}

	
	
}
