package it.unisannio.weather.exceptions;

public class NegativeAltitudeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NegativeAltitudeException() {
		super();
	}

	public NegativeAltitudeException(String message) {
		super(message);
	}
	
}
