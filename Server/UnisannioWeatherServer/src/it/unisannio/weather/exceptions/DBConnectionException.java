package it.unisannio.weather.exceptions;

public class DBConnectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DBConnectionException() {
		super();
	}

	public DBConnectionException(String arg0) {
		super(arg0);
	}
	
}
