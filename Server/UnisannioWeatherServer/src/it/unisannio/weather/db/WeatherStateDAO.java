package it.unisannio.weather.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;

import it.unisannio.weather.domain.measures.Attribute;
import it.unisannio.weather.domain.measures.Attribute.AttributeName;
import it.unisannio.weather.domain.measures.BMP180_Measure;
import it.unisannio.weather.domain.measures.DHT11_Measure;
import it.unisannio.weather.domain.measures.HumidityAttribute;
import it.unisannio.weather.domain.measures.LDR_VT900_N3_Measure;
import it.unisannio.weather.domain.measures.LightAttribute;
import it.unisannio.weather.domain.measures.Measure;
import it.unisannio.weather.domain.measures.Measure.MeasureName;
import it.unisannio.weather.domain.measures.PressureAttribute;
import it.unisannio.weather.domain.measures.TemperatureAttribute;
import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.exceptions.InvalidAttributeException;
import it.unisannio.weather.exceptions.TimeFormatException;
import it.unisannio.weather.utils.TimeStamp;

public class WeatherStateDAO implements DAOInterface<WeatherState>{

	public static final String INSERT_STATEMENT = "insert into weather_state (city, timestamp, humidity, light, pressure, temperature) values (?, ?, ?, ?, ?, ?);";
	public static final String UPDATE_STATEMENT = "update weather_state set city=?, timestamp=?, humidity=?, light=?, pressure=?, temperature=?";
	public static final String DELETE_STATEMENT = "delete from weather_state where city=? and timestamp = ?;";
	public static final String GET_STATEMENT = "select * from weather_state where city=? and timestamp=?";
	public static final String GET_LAST_STATEMENT = "select * from weather_state where idweather_state = (select max(idweather_state) from weather_state where city = ?)";
	public static final String GET_ALL_STATEMENT = "select * from weather_state where city = ?";
	public static final String CITY_COLUMN = "city";
	public static final String TIMESTAMP_COLUMN = "timestamp";
	public static final String HUMIDITY_COLUMN = "humidity";
	public static final String LIGHT_COLUMN = "light";
	public static final String PRESSURE_COLUMN = "pressure";
	public static final String TEMPERATURE_COLUMN = "temperature";
	private Connection connection;
	
	public WeatherStateDAO(Connection connection) {
		this.connection = connection;
	}

	@Override
	public void insert(WeatherState object) throws SQLException {
		int humidity = 0;
		double temperature = 0, light = 0, pressure = 0;
		String query = new String(INSERT_STATEMENT);
		ArrayList<Measure> ms = (ArrayList<Measure>) object.getMeasures();
		for(Measure m:ms){
			if(m.getName()==MeasureName.DHT11){
				humidity = (int) m.getAttributes().get(0).getValue();
			}
			else if(m.getName()==MeasureName.BMP180){
				List<Attribute> attr = m.getAttributes();
				for(Attribute a: attr){
					if(a.getName()==AttributeName.TEMPERATURE)
						temperature = a.getValue();
					else if(a.getName()==AttributeName.PRESSURE)
						pressure = a.getValue();
//					else if(a.getName()==AttributeName.ALTITUDE)
//						altitude = a.getValue();
				}
			}
			else{
				//LDR_VT900_N3 light
				light = m.getAttributes().get(0).getValue();
			}
		}
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, object.getCityName());
		ps.setString(2, object.getTime().toString());
		ps.setInt(3, humidity);
		ps.setDouble(4, light);
		ps.setDouble(5, pressure);
		ps.setDouble(6, temperature);
		ps.executeUpdate();
	}

	@Override
	public void update(WeatherState object) throws SQLException {
//		int humidity = 0;
//		double temperature = 0, light = 0, pressure = 0;
//		ArrayList<Measure> ms = (ArrayList<Measure>) object.getMeasures();
//		for(Measure m:ms){
//			if(m.getName()==MeasureName.DHT11){
//				humidity = (int) m.getAttributes().get(0).getValue();
//			}
//			else if(m.getName()==MeasureName.BMP180){
//				List<Attribute> attr = m.getAttributes();
//				for(Attribute a: attr){
//					if(a.getName()==AttributeName.TEMPERATURE)
//						temperature = a.getValue();
//					else if(a.getName()==AttributeName.PRESSIURE)
//						pressure = a.getValue();
//				}
//			}
//			else{
//				//LDR_VT900_N3 light
//				light = m.getAttributes().get(0).getValue();
//			}
//		}
//		String query = new String(UPDATE_STATEMENT);
//		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
//		ps.setString(1, object.getCityName());
//		ps.setString(2, object.getTime().toString());
//		ps.setInt(3, humidity);
//		ps.setDouble(4, light);
//		ps.setDouble(5, pressure);
//		ps.setDouble(6, temperature);
//		ps.setDouble(7,object.getRainOdds());
//		ps.executeUpdate();
//		
	}

	@Override
	public void delete(String... keys) throws SQLException {
		String query = new String(DELETE_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ps.setString(2, keys[1]);
		ps.executeUpdate();
		
	}

	@Override
	public WeatherState getByKey(String... keys) throws SQLException {
		String query = new String(GET_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ps.setString(2, keys[1]);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		List<Measure> measures = new ArrayList<Measure>();
		String city ="", columnName;
		double light = 0,pressure = 0,temperature = 0;
		int humidity = 0;
		String time = null;
		boolean found = false;
		while(rs.next()){
			found = true;
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(CITY_COLUMN))
					city = rs.getString(i+1);
				else if(columnName.equalsIgnoreCase(TIMESTAMP_COLUMN)){
					time = rs.getString(i+1);
				}
				else if(columnName.equalsIgnoreCase(HUMIDITY_COLUMN)){
					humidity = rs.getInt(i+1);
				}
				else if(columnName.equalsIgnoreCase(LIGHT_COLUMN)){
					light = Math.floor(rs.getDouble(i+1)*100)/100;
				}
				else if(columnName.equalsIgnoreCase(PRESSURE_COLUMN)){
					pressure = Math.floor(rs.getDouble(i+1)*100)/100;
				}
				else if(columnName.equalsIgnoreCase(TEMPERATURE_COLUMN)){
					temperature = Math.floor(rs.getDouble(i+1)*100)/100;
				}
			}
		}
		if(found){
			TimeStamp t = null;
			try {
				t = TimeStamp.getTimeStampFromString(time);
			} catch (TimeFormatException e1) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			WeatherState wt =  new WeatherState(city, t);
			HumidityAttribute ha = new HumidityAttribute(humidity);
			LightAttribute la = new LightAttribute(light);
			PressureAttribute pa = new PressureAttribute(pressure);
			TemperatureAttribute ta = new TemperatureAttribute(temperature);
			BMP180_Measure bm = new BMP180_Measure();
			DHT11_Measure dt = new DHT11_Measure();
			LDR_VT900_N3_Measure lm = new LDR_VT900_N3_Measure();
			try {
				bm.addAttribute(ta);
				bm.addAttribute(pa);
				dt.addAttribute(ha);
				lm.addAttribute(la);
			} catch (InvalidAttributeException e) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			
			measures.add(bm);
			measures.add(dt);
			measures.add(lm);
			wt.setMeasures(measures);
			return wt;
		}
		return null;
	}
	
	public WeatherState getLastWeatherStateByCity(String city) throws SQLException{
		String query = new String(GET_LAST_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, city);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String columnName;
		List<Measure> measures = new ArrayList<Measure>();
		double light = 0,pressure = 0,temperature = 0;
		int humidity = 0;
		String time = null;
		boolean found = false;
		while(rs.next()){
			found = true;
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(CITY_COLUMN))
					city = rs.getString(i+1);
				else if(columnName.equalsIgnoreCase(TIMESTAMP_COLUMN)){
					time = rs.getString(i+1);
				}
				else if(columnName.equalsIgnoreCase(HUMIDITY_COLUMN)){
					humidity = rs.getInt(i+1);
				}
				else if(columnName.equalsIgnoreCase(LIGHT_COLUMN)){
					light = Math.floor(rs.getDouble(i+1)*100)/100;
				}
				else if(columnName.equalsIgnoreCase(PRESSURE_COLUMN)){
					pressure = Math.floor(rs.getDouble(i+1)*100)/100;
				}
				else if(columnName.equalsIgnoreCase(TEMPERATURE_COLUMN)){
					temperature = Math.floor(rs.getDouble(i+1)*100)/100;
				}
			}
		}
		if(found){
			TimeStamp t = null;
			try {
				t = TimeStamp.getTimeStampFromString(time);
			} catch (TimeFormatException e1) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			WeatherState wt =  new WeatherState(city, t);
			HumidityAttribute ha = new HumidityAttribute(humidity);
			LightAttribute la = new LightAttribute(light);
			PressureAttribute pa = new PressureAttribute(pressure);
			TemperatureAttribute ta = new TemperatureAttribute(temperature);
			BMP180_Measure bm = new BMP180_Measure();
			DHT11_Measure dt = new DHT11_Measure();
			LDR_VT900_N3_Measure lm = new LDR_VT900_N3_Measure();
			try {
				bm.addAttribute(ta);
				bm.addAttribute(pa);
				dt.addAttribute(ha);
				lm.addAttribute(la);
			} catch (InvalidAttributeException e) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			measures.add(bm);
			measures.add(dt);
			measures.add(lm);
			wt.setMeasures(measures);
			return wt;
		}
		return null;
	}
	
	public List<WeatherState> getAllWeatherStateByCity(String cityName) throws SQLException{
		List<WeatherState> weatherlist = new ArrayList<WeatherState>();
		String query = new String(GET_ALL_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, cityName);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String columnName;
		List<Measure> measures;
		double light = 0,pressure = 0,temperature = 0;
		int humidity = 0;
		String time = null;
		WeatherState wt;
		while(rs.next()){
			wt =  new WeatherState();
			measures = new ArrayList<Measure>();
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(CITY_COLUMN));
					//city = rs.getString(i+1);
				else if(columnName.equalsIgnoreCase(TIMESTAMP_COLUMN)){
					time = rs.getString(i+1);
				}
				else if(columnName.equalsIgnoreCase(HUMIDITY_COLUMN)){
					humidity = rs.getInt(i+1);
				}
				else if(columnName.equalsIgnoreCase(LIGHT_COLUMN)){
					light = Math.floor(rs.getDouble(i+1)*100)/100;
				}
				else if(columnName.equalsIgnoreCase(PRESSURE_COLUMN)){
					pressure = Math.floor(rs.getDouble(i+1)*100)/100;
				}
				else if(columnName.equalsIgnoreCase(TEMPERATURE_COLUMN)){
					temperature = Math.floor(rs.getDouble(i+1)*100)/100;
				}
			}
			TimeStamp t = null;
			try {
				t = TimeStamp.getTimeStampFromString(time);
			} catch (TimeFormatException e1) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			HumidityAttribute ha = new HumidityAttribute(humidity);
			LightAttribute la = new LightAttribute(light);
			PressureAttribute pa = new PressureAttribute(pressure);
			TemperatureAttribute ta = new TemperatureAttribute(temperature);
			BMP180_Measure bm = new BMP180_Measure();
			DHT11_Measure dt = new DHT11_Measure();
			LDR_VT900_N3_Measure lm = new LDR_VT900_N3_Measure();
			try {
				bm.addAttribute(ta);
				bm.addAttribute(pa);
				dt.addAttribute(ha);
				lm.addAttribute(la);
			} catch (InvalidAttributeException e) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			measures.add(bm);
			measures.add(dt);
			measures.add(lm);
			wt.setMeasures(measures);
			wt.setTime(t);
			wt.setCityName(cityName);
			weatherlist.add(wt);
		}
		return weatherlist;
	}

}
