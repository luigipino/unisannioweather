package it.unisannio.weather.db;

import java.sql.SQLException;

//This interface shows all methods related to the CRUD operation for a Java Object towards a relational DB
public interface DAOInterface<T> {

	public void insert(T object) throws SQLException;
	
	public void update(T object) throws SQLException;
	
	public void delete(String... keys) throws SQLException;
	
	public T getByKey(String... keys) throws SQLException;
}
