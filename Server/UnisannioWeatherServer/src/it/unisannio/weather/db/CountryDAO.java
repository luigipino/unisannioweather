package it.unisannio.weather.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;
import it.unisannio.weather.domain.places.Country;

public class CountryDAO implements DAOInterface<Country> {


	public static final String INSERT_STATEMENT = "insert into country (id, name) values (?, ?);";
	public static final String UPDATE_STATEMENT = "update country set id = ? , name = ? where id = ?;";
	public static final String DELETE_STATEMENT = "delete from country where id=?;";
	public static final String GET_STATEMENT = "select * from country where id=?";
	public static final String ID_COLUMN = "id";
	public static final String NAME_COLUMN = "name";
	private Connection connection;
	
	public CountryDAO(Connection connection){
		this.connection = connection;
	}

	@Override
	public void insert(Country object) throws SQLException {
		String query = new String(INSERT_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, object.getId());
		ps.setString(2, object.getName());
		ps.executeUpdate();
	}

	@Override
	public void update(Country object) throws SQLException {
		String query = new String(UPDATE_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, object.getId());
		ps.setString(2, object.getName());
		ps.setString(3, object.getId());
		ps.executeUpdate();
	}

	@Override
	public void delete(String... keys) throws SQLException {
		String query = new String(DELETE_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ps.executeUpdate();
	}

	@Override
	public Country getByKey(String... keys) throws SQLException {
		String query = new String(GET_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String id ="",name = "", columnName;
		boolean found = false;
		while(rs.next()){
			found = true;
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(ID_COLUMN))
					id = rs.getString(i+1);
				else if(columnName.equalsIgnoreCase(NAME_COLUMN)){
					name = rs.getString(i+1);
				}
			}
		}
		if(found){
			return new Country(name,id);
		}
		return null;
	}
	
}
