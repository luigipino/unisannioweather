package it.unisannio.weather.db;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

import it.unisannio.weather.exceptions.DBConnectionException;

//This class manages all the DAO objects, in order to execute the CRUD operation for an Object using a single Point of Access
public class DAOManager {

	private static DAOManager dm = null;
	private String url,username,password;
	private CityDAO city;
	private RegionDAO region;
	private CountryDAO country;
	private WeatherStateDAO weatherState;
	protected Connection connection;
	private static final String MYSQL_DB_DRIVER = "com.mysql.jdbc.Driver";

	private DAOManager(String url,String username,String password){
		city = null;
		region = null;
		country = null;
		weatherState = null;
		this.url = url;
		this.username = username;
		this.password = password;
	}
	
	public static DAOManager getInstance(String url,String username,String password) throws ClassNotFoundException, SQLException{
		if(dm==null){
			Class.forName(MYSQL_DB_DRIVER);
			dm = new DAOManager(url, username, password);
			dm.connect();
		}
		return dm;
	}
	
	private void connect() throws SQLException{
		connection = (Connection) DriverManager.getConnection(url,username,password);
	}

	private void disconnect() throws SQLException{
		connection.close();
	}

	public void destroy() throws SQLException{
		dm.disconnect();
		dm = null;
	}
	
	public CityDAO getCityDAO() throws DBConnectionException{
		if(connection==null)
			throw new DBConnectionException("Connection is not set yet");
		if(city == null)
			city = new CityDAO(connection);
		return city;
	}

	public RegionDAO getRegionDAO() throws DBConnectionException{
		if(connection==null)
			throw new DBConnectionException("Connection is not set yet");
		if(region == null)
			region = new RegionDAO(connection);
		return region;
	}
	public CountryDAO getCountryDAO() throws DBConnectionException{
		if(connection==null)
			throw new DBConnectionException("Connection is not set yet");
		if(country == null)
			country = new CountryDAO(connection);
		return country;
	}
	
	public WeatherStateDAO getWeatherStateDAO() throws DBConnectionException{
		if(connection==null)
			throw new DBConnectionException("Connection is not set yet");
		if(weatherState == null)
			weatherState = new WeatherStateDAO(connection);
		return weatherState;
	}

}
