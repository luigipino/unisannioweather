package it.unisannio.weather.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;

import it.unisannio.weather.domain.places.City;
import it.unisannio.weather.exceptions.NegativeAltitudeException;

public class CityDAO implements DAOInterface<City> {

	public static final String INSERT_STATEMENT = "insert into city (name, region, altitude, latitude) values (?, ?, ?, ?);";
	public static final String UPDATE_STATEMENT = "update city set name = ? , region = ?, altitude = ?, latitude = ? where name = ?;";
	public static final String DELETE_STATEMENT = "delete from city where name=?;";
	public static final String GET_STATEMENT = "select * from city where name=?";
	public static final String SELECT_ALL_STATEMENT = "select * from city";
	public static final String NAME_COLUMN = "name";
	public static final String REGION_COLUMN = "region";
	public static final String ALTITUDE_COLUMN = "altitude";
	public static final String LATITUDE_COLUMN = "latitude";
	private Connection connection;

	public CityDAO(Connection connection){
		this.connection = connection;
	}

	@Override
	public void insert(City object) throws SQLException {
		String query = new String(INSERT_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, object.getName());
		ps.setString(2, object.getRegionName());
		double alt;
		try {
			alt = object.getAltitude();
		} catch (NegativeAltitudeException e) {
			alt = 0;
		}
		ps.setDouble(3, alt);
		ps.setDouble(4, object.getLatitude());
		ps.executeUpdate();
	}

	@Override
	public void update(City object) throws SQLException {
		String query = new String(UPDATE_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, object.getName());
		ps.setString(2, object.getRegionName());
		double alt;
		try {
			alt = object.getAltitude();
		} catch (NegativeAltitudeException e) {
			alt = 0;
		}
		ps.setDouble(3, alt);
		ps.setDouble(4, object.getLatitude());
		ps.setString(5, object.getName());
		ps.executeUpdate();

	}

	@Override
	public void delete(String... keys) throws SQLException {
		String query = new String(DELETE_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ps.executeUpdate();
	}

	@Override
	public City getByKey(String... keys) throws SQLException {
		String query = new String(GET_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String name = "",regionName = "",columnName;
		double altitude = -1.0, latitude=0.0;
		boolean found = false;
		while(rs.next()){
			found = true;
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(NAME_COLUMN))
					name = rs.getString(i+1);
				else if(columnName.equalsIgnoreCase(REGION_COLUMN)){
					regionName = rs.getString(i+1);
				}
				else if(columnName.equalsIgnoreCase(ALTITUDE_COLUMN)){
					altitude = rs.getDouble(i+1);
				}
				else if(columnName.equalsIgnoreCase(LATITUDE_COLUMN)){
					latitude = rs.getDouble(i+1);
				}
			}
		}
		if(found){
			City city = new City(name,regionName,latitude);
			try {
				city.setAltitude(altitude);
			} catch (NegativeAltitudeException e) {
				//l'eccezione non dovrebbe mai essere lanciata perch� il valore inserito nel DB � sicuramente corretto
			}
			return city;
		}
		return null;
	}
	
	public List<City> selectAll() throws SQLException{
		List<City> cities = new ArrayList<City>();
		String query = new String(SELECT_ALL_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String columnName;
		City c;
		while(rs.next()){
			c = new City();
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(NAME_COLUMN))
					c.setName(rs.getString(i+1));
				else if(columnName.equalsIgnoreCase(REGION_COLUMN)){
					c.setRegionName(rs.getString(i+1));
				}
				else if(columnName.equalsIgnoreCase(ALTITUDE_COLUMN)){
					try {
						c.setAltitude(rs.getDouble(i+1));
					} catch (NegativeAltitudeException e) {
					}
				}
				else if(columnName.equalsIgnoreCase(LATITUDE_COLUMN)){
					c.setLatitude(rs.getDouble(i+1));
				}
			}
			cities.add(c);
		}
		return cities;
	}
	
}
