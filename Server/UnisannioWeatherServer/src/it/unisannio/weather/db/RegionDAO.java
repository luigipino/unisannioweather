package it.unisannio.weather.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;

import it.unisannio.weather.domain.places.Region;
public class RegionDAO implements DAOInterface<Region> {

	public static final String INSERT_STATEMENT = "insert into region (name, country) values (?, ?);";
	public static final String DELETE_STATEMENT = "delete from region where name=? and country=? ";
	public static final String GET_STATEMENT = "select * from region where name=? and  country=?";
	public static final String SELECT_ALL_STATEMENT = "select * from region";
	public static final String NAME_COLUMN = "name";
	public static final String COUNTRY_COLUMN = "country";
	
	private Connection connection;
	
	public RegionDAO(Connection connection) {
		this.connection = connection;
	}

	@Override
	public void insert(Region object) throws SQLException {
		String query = new String(INSERT_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, object.getName());
		ps.setString(2, object.getCountryName());
		ps.executeUpdate();
		
	}

	@Override
	public void update(Region object) throws SQLException {
	}

	@Override
	public void delete(String... keys) throws SQLException {
		String query = new String(DELETE_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ps.setString(2, keys[1]);
		ps.executeUpdate();
		
	}

	@Override
	public Region getByKey(String... keys) throws SQLException {
		String query = new String(GET_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ps.setString(1, keys[0]);
		ps.setString(2, keys[1]);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String name ="",country = "", columnName;
		boolean found = false;
		while(rs.next()){
			found = true;
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(NAME_COLUMN))
					name = rs.getString(i+1);
				else if(columnName.equalsIgnoreCase(COUNTRY_COLUMN)){
					country = rs.getString(i+1);
				}
			}
		}
		if(found){
			return new Region(name, country);
		}
		return null;
	}

	public Map<String, Region> selectAll() throws SQLException {
		Map<String, Region> regions = new HashMap<String, Region>();
		String query = new String(SELECT_ALL_STATEMENT);
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(query);
		ResultSet rs =  ps.executeQuery();
		ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
		String columnName;
		Region r;
		while(rs.next()){
			r = new Region();
			for(int i = 0;i<rsmd.getColumnCount();i++){
				columnName = rsmd.getColumnName(i+1);
				if(columnName.equalsIgnoreCase(NAME_COLUMN))
					r.setName(rs.getString(i+1));
				else if(columnName.equalsIgnoreCase(COUNTRY_COLUMN)){
					r.setCountryName(rs.getString(i+1));
				}
			}
			regions.put(r.getName(), r);
		}
		return regions;
	}


}
