package it.unisannio.weather.server.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unisannio.weather.db.DAOManager;
import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.domain.places.City;
import it.unisannio.weather.exceptions.DBConnectionException;
import it.unisannio.weather.utils.Constants;
import it.unisannio.weather.utils.WeatherAlgorithms;

/**
 * Servlet implementation class Request
 */
@WebServlet("/WeatherServlet")
public class WeatherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeatherServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String citySearched = request.getParameter("city");
		StringTokenizer st = new StringTokenizer(citySearched, ",");
		String city = st.nextToken();
		City c = null;
			try {
				c = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getCityDAO().getByKey(city);
			} catch (ClassNotFoundException | SQLException | DBConnectionException e1) {
				RequestDispatcher rd = request.getRequestDispatcher("error.html");
				rd.forward(request, response);
				return;
			}
		if(c==null){
			RequestDispatcher rd = request.getRequestDispatcher("unsupported-city.html");
			rd.forward(request, response);
			return;
		}
		st.nextToken(); //skip region
		String country = st.nextToken().trim();
		String regionName = "";
		
			try {
				regionName = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getRegionDAO().getByKey(c.getRegionName(), country).toString();
			} catch (ClassNotFoundException | SQLException | DBConnectionException e1) {
				RequestDispatcher rd = request.getRequestDispatcher("error.html");
				rd.forward(request, response);
				return;
			}
		
		WeatherState ws = null;
	
			try {
				ws = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getWeatherStateDAO().getLastWeatherStateByCity(city);
			} catch (ClassNotFoundException | SQLException | DBConnectionException e) {
				RequestDispatcher rd = request.getRequestDispatcher("error.html");
				rd.forward(request, response);
				return;
			}
		c.setTodayLightHours(WeatherAlgorithms.getLightHours(c.getLatitude(), GregorianCalendar.getInstance().get(GregorianCalendar.DAY_OF_YEAR)));
		c.setCurrentWeatherState(ws);
		request.setAttribute("city", c);
		request.setAttribute("region", regionName);
		RequestDispatcher rd = request.getRequestDispatcher("weather-info.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
