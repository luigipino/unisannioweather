package it.unisannio.weather.server.servlet;

import java.util.GregorianCalendar;

import it.unisannio.weather.utils.TimeStamp;

public class Test {

	public static void main(String[] args) {
		GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		gc.set(GregorianCalendar.MONTH, 2);
		gc.set(GregorianCalendar.YEAR, 2000);
		gc.set(GregorianCalendar.HOUR_OF_DAY, 1);
		gc.set(GregorianCalendar.MINUTE, 1);
		TimeStamp ts = new TimeStamp(gc);
		System.out.println(ts.toString());
	}
}
