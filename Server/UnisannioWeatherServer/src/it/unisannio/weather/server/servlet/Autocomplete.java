package it.unisannio.weather.server.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unisannio.weather.db.DAOManager;
import it.unisannio.weather.domain.places.City;
import it.unisannio.weather.domain.places.Region;
import it.unisannio.weather.exceptions.DBConnectionException;
import it.unisannio.weather.utils.Constants;
import net.sourceforge.ajaxtags.servlets.BaseAjaxServlet;
import net.sourceforge.ajaxtags.xml.AjaxXmlBuilder;

/**
 * Servlet implementation class Autocomplete
 */
@WebServlet("/Autocomplete")
public class Autocomplete extends BaseAjaxServlet {
	private static final long serialVersionUID = 1L;
//	public static String[] cities = {"Benevento",
//			"Bari", "Roma", "Alessandria", "Montesarchio",
//			"Catania", "Milano"			
//	};
	private List<City> cities;
	private Map<String, Region> regions;
	
	
	@Override
	public String getXmlContent(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String input = req.getParameter("input");
		String city = req.getParameter("city");
		switch (input){		
		case "search": return getCity(city); 
		case "other": return doOther(city);
		default: return null;
		}
	}

	public Autocomplete() throws ClassNotFoundException, SQLException, DBConnectionException {
		super();
		DAOManager dm = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD);
		cities = dm.getCityDAO().selectAll();
		regions = dm.getRegionDAO().selectAll();
		dm.destroy();
	}

	public String getCity(String city){
		AjaxXmlBuilder ab =  new AjaxXmlBuilder();
		String cityName;
		Region r;
		for(City c : cities){
			cityName = c.getName();
			r = regions.get(c.getRegionName());
			if(cityName.toUpperCase().startsWith(city.toUpperCase())){
				ab.addItem(cityName+", "+r.getName()+", "+r.getCountryName(), cityName+", "+r.getName()+", "+r.getCountryName());
			}
		}	
		return ab.toString();
	}
	
	public String doOther(String input){
		AjaxXmlBuilder ab =  new AjaxXmlBuilder();
		ab.addItem("nothing", "");
		return ab.toString();
	}
}
