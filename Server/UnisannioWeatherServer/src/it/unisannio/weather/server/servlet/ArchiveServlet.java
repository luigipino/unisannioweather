package it.unisannio.weather.server.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unisannio.weather.db.DAOManager;
import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.domain.places.City;
import it.unisannio.weather.exceptions.DBConnectionException;
import it.unisannio.weather.exceptions.NegativeAltitudeException;
import it.unisannio.weather.utils.Constants;

/**
 * Servlet implementation class ArchieveServlet
 */
@WebServlet("/ArchiveServlet")
public class ArchiveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArchiveServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String cityName = (String) request.getParameter("city");
		StringTokenizer st = new StringTokenizer(cityName, ",");
		String city = st.nextToken().trim();
		City c = null;
			try {
				c = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getCityDAO().getByKey(city);
			} catch (ClassNotFoundException | SQLException | DBConnectionException e1) {
				RequestDispatcher rd = request.getRequestDispatcher("error.html");
				rd.forward(request, response);
				return;
			}
		 
		if(c==null){
			RequestDispatcher rd = request.getRequestDispatcher("unsupported-city.html");
			rd.forward(request, response);
			return;
		}
//		String region = request.getParameter("region");
		String region = st.nextToken()+", "+st.nextToken();
		request.setAttribute("region", region);
		try {
			request.setAttribute("altitude", c.getAltitude());
		} catch (NegativeAltitudeException e1) {
		}
		List<WeatherState> weatherList = null;
		try {
			 weatherList = DAOManager.getInstance(Constants.MYSQL_DB_URL, Constants.MYSQL_DB_USERNAME, Constants.MYSQL_DB_PASSWORD).getWeatherStateDAO().getAllWeatherStateByCity(city);
		} catch (ClassNotFoundException | SQLException | DBConnectionException e) {
			RequestDispatcher rd = request.getRequestDispatcher("error.html");
			rd.forward(request, response);
			return;
		}
		request.setAttribute("weatherList", weatherList);
		request.setAttribute("cityName",city);
		RequestDispatcher rd = request.getRequestDispatcher("weather-archive.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
