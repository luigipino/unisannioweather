package it.unisannio.weather.domain.measures;

public class HumidityAttribute extends Attribute{

	public HumidityAttribute(double value){
		super(AttributeName.HUMIDITY, value);
	}
	
}
