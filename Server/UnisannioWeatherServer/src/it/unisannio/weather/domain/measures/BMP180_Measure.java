package it.unisannio.weather.domain.measures;

import it.unisannio.weather.domain.measures.Attribute.AttributeName;
import it.unisannio.weather.exceptions.InvalidAttributeException;

public class BMP180_Measure extends Measure{

	public BMP180_Measure(){
		super(MeasureName.BMP180);
	}
	
	@Override
	public void addAttribute(Attribute a) throws InvalidAttributeException {
		if(a.getName()==AttributeName.PRESSURE || a.getName()==AttributeName.TEMPERATURE || a.getName()==AttributeName.ALTITUDE)
			super.addAttribute(a);
		else
			throw new InvalidAttributeException("BMP180 Measure must be only Pressure, Temperature or Altitude");
	}
	
}
