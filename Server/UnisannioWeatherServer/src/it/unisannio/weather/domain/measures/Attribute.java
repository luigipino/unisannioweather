package it.unisannio.weather.domain.measures;

public abstract class Attribute {

	private AttributeName name;
	private double value;
	
	public Attribute(AttributeName name, double value) {
		super();
		this.name = name;
		this.value = value;
	}

	public AttributeName getName() {
		return name;
	}

	public void setName(AttributeName name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public enum AttributeName{
		TEMPERATURE,PRESSURE, LIGHT, HUMIDITY, ALTITUDE;
	}
	
}
