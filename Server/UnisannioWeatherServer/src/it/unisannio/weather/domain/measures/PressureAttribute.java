package it.unisannio.weather.domain.measures;

public class PressureAttribute extends Attribute{

	public PressureAttribute(double value) {
		super(AttributeName.PRESSURE, value);
	}
	
}
