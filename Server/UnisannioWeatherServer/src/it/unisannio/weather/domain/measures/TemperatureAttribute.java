package it.unisannio.weather.domain.measures;

public class TemperatureAttribute extends Attribute{

	public TemperatureAttribute(double value) {
		super(AttributeName.TEMPERATURE, value);
	}
	
}
