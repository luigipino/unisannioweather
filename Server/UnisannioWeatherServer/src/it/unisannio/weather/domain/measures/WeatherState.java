package it.unisannio.weather.domain.measures;

import java.util.ArrayList;
import java.util.List;

import it.unisannio.weather.domain.measures.Attribute.AttributeName;
import it.unisannio.weather.exceptions.OutOfRangeException;
import it.unisannio.weather.utils.TimeStamp;
import it.unisannio.weather.utils.WeatherAlgorithms;

public class WeatherState {

	private String cityName;
	private TimeStamp time;
	private double rainOdds;
	private List<Measure> measures;
	
	//Constructor of WeatherState
	public WeatherState(String cityName,TimeStamp time){
		this.time = time;
		this.cityName = cityName;
		rainOdds = 0.0;
		measures = new ArrayList<Measure>();
	}

	//This costructor is used only for the db reading
	public WeatherState() {
		measures = new ArrayList<Measure>();
		rainOdds = 0.0;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public TimeStamp getTime() {
		return time;
	}

	public void setTime(TimeStamp time) {
		this.time = time;
	}

	public double getRainOdds() {
		return rainOdds;
	}

	public void setRainOdds(double rainOdds) throws OutOfRangeException {
		if(rainOdds>=0 && rainOdds<=1)
			this.rainOdds = rainOdds;
		else throw new OutOfRangeException("Rain Odds must be a number between 0 and 1");
	}

	public List<Measure> getMeasures() {
		return measures;
	}

	//This method sets the measures readed by sensors
	public void setMeasures(List<Measure> measures) {
		double t = 0;
		int h = 0;
		for(Measure m: measures){
			for(Attribute a: m.getAttributes()){
				if(a.getName().equals(AttributeName.TEMPERATURE)){
					t = a.getValue();
				}
				if(a.getName().equals(AttributeName.HUMIDITY)){
					h = (int) a.getValue();
				}
			}
		}
		rainOdds = WeatherAlgorithms.calculateRainOdds(t, h)*100;
		this.measures = measures;
	}
	
	public void addMeasure(Measure measure){
		measures.add(measure);
	}
	
}
