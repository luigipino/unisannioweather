package it.unisannio.weather.domain.measures;

import java.util.ArrayList;
import java.util.List;

import it.unisannio.weather.exceptions.InvalidAttributeException;

public abstract class Measure {
	
	private List<Attribute> attributes;
	private MeasureName name;
	
	public Measure(MeasureName name){
		this.name = name; 
		attributes = new ArrayList<Attribute>();
	}
	
	public MeasureName getName() {
		return name;
	}
	
	public void setName(MeasureName name) {
		this.name = name;
	}

	public void addAttribute(Attribute a) throws InvalidAttributeException{
		attributes.add(a);
	}
	
	public List<Attribute> getAttributes() {
		return attributes;
	}

	public enum MeasureName{
		DHT11,BMP180,LDR_VT900_N3
	}
	
}
