package it.unisannio.weather.domain.measures;

public class AltitudeAttribute extends Attribute{

	public AltitudeAttribute(double value) {
		super(AttributeName.ALTITUDE, value);
	}

}
