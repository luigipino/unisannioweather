package it.unisannio.weather.domain.measures;

public class LightAttribute extends Attribute{

	public LightAttribute(double value) {
		super(AttributeName.LIGHT, value);
	}

}
