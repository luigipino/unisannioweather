package it.unisannio.weather.domain.measures;

import it.unisannio.weather.domain.measures.Attribute.AttributeName;
import it.unisannio.weather.exceptions.InvalidAttributeException;

public class LDR_VT900_N3_Measure extends Measure{

	public LDR_VT900_N3_Measure() {
		super(MeasureName.LDR_VT900_N3);
	}
	
	@Override
	public void addAttribute(Attribute a) throws InvalidAttributeException {
		if(a.getName()==AttributeName.LIGHT)
			super.addAttribute(a);
		else
			throw new InvalidAttributeException("LDR VT900 N3 Measure must be only Light");
	}
	
	
}
