package it.unisannio.weather.domain.places;

import java.util.ArrayList;
import java.util.List;

public class Country {

	private String name,id;
	private List<Region> regions;
	
	public Country(String name, String id) {
		super();
		this.name = name;
		this.id = id;
		this.regions = new ArrayList<Region>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}
	
	public void addRegion(Region region){
		regions.add(region);
	}
	
}
