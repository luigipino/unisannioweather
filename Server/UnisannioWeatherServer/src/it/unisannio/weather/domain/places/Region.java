package it.unisannio.weather.domain.places;

import java.util.ArrayList;
import java.util.List;

public class Region {

	private String countryName;
	private String name;
	private List<City> cities;
	
	public Region(String name,String countryName){
		this.name = name;
		this.countryName = countryName;
		this.cities = new ArrayList<City>();
	}

	public Region() {
		super();
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	
	public void addCity(City city){
		cities.add(city);
	}
	
	@Override
	public String toString() {
		return name+", "+countryName;
	}
	
}
