package it.unisannio.weather.domain.places;

import java.util.Calendar;
import java.util.GregorianCalendar;

import it.unisannio.weather.domain.measures.WeatherState;
import it.unisannio.weather.exceptions.NegativeAltitudeException;
import it.unisannio.weather.utils.WeatherAlgorithms;

public class City {

	private String name, regionName, todayLightHours;
	double latitude, altitude;
	private WeatherState currentWeatherState;
//	private static final String STRING_TIME_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
//	private Pattern timePattern;

	
	
	public City(String name,String regionName,double latitude){
		super();
		this.name = name;
		this.regionName = regionName;
		this.latitude = latitude;
		this.todayLightHours = WeatherAlgorithms.getLightHours(latitude, GregorianCalendar.getInstance().get(Calendar.DAY_OF_YEAR));
		this.currentWeatherState = null;
	//	timePattern = Pattern.compile(STRING_TIME_PATTERN);
		altitude = -1.0;
	}
	public City() {
		super();
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public double getAltitude() throws NegativeAltitudeException {
		if(altitude>0)
			return altitude;
		throw new NegativeAltitudeException("Altitude not set yet!");
	}

	public void setAltitude(double altitude) throws NegativeAltitudeException{
		checkAltitude(altitude);
		this.altitude = altitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public String getTodayLightHours(){
		return todayLightHours;
	}

	public void setTodayLightHours(String todayLightHours){
		this.todayLightHours = todayLightHours;
	}

	public WeatherState getCurrentWeatherState(){
		return currentWeatherState;
	}

	public void setCurrentWeatherState(WeatherState currentWeatherState) {
		this.currentWeatherState = currentWeatherState;
	}

	private void checkAltitude(double altitude) throws NegativeAltitudeException{
		if(altitude<=0) throw new NegativeAltitudeException("The altitude must be a non negative number!");
	}

//	private void checkTimeFormat(String time) throws TimeFormatException{
//		Matcher matcher = timePattern.matcher(time);
//		if(!matcher.matches()) throw new TimeFormatException("Wrong Time Format!");
//	}

}
