package it.unisannio.weather.utils;

public class WeatherAlgorithms {
	
	public static final double RAIN_ODDS_BY_ONE_CELSIUS = 5;
	
	//calculate a city precipitation probability based on its temperature and humidity
	//first of all we calculate the dewPoint as the temperature in which we have 100% rain probability
	//we choose 2.5% for each difference Celsius grade between the dew point and the measured temperature
	public static double calculateRainOdds(double temperature, int humidity){
		double a = 17.271, b = 237.7;
		double temp = ((a * temperature) / (b + temperature)) + Math.log(humidity*0.01);
		double dewPoint = (b * temp) / (a - temp);
		double diff = temperature - dewPoint;
	
		if(diff<=20 && diff >=0)
			return 1-(diff*RAIN_ODDS_BY_ONE_CELSIUS*0.01);
		else
			return 0;

	}
	//calculate city lightHours based on its latitude and days of the year.
	//P = asin[.39795*cos(.2163108 + 2*atan{.9671396*tan[.00860(J-186)]})]
	//        _                                         _
	//        				/ sin(0.8333*pi/180) + sin(L*pi/180)*sin(P) \
	//D = 24 - (24/pi)*acos{  -----------------------------------------  }
	//        				\_          cos(L*pi/180)*cos(P)           _/
	public static String getLightHours(double latitude,int dayOfTheYear){
		double p = Math.asin(0.39795*(Math.cos(0.2163108 + 2* Math.atan(0.9671396*Math.tan(0.00860*(dayOfTheYear-186))))));
		double num = (Math.sin(0.8333*Math.PI/180) + Math.sin(latitude*Math.PI/180)* Math.sin(p)); 
		double den = (Math.cos(latitude*Math.PI/180)*Math.cos(p));
		double d = (24 - (24/Math.PI)* Math.acos(num/den));
		return getHoursAndMins(Math.floor(d*100)/100);
	}
	
	private static String getHoursAndMins(double d){
		int hours = (int) d;
		int mins = (int) ((d - hours)*60);
		return hours+" h "+mins+" m";
	}
	
	
}
