package it.unisannio.weather.utils;

import java.io.File;

public class Constants {
	
	//Arduino_Path luigi
	public static final String ARDUINO_COMMAND_PATH = "D:"+File.separator+"Programmi"+File.separator+"Arduino"+File.separator;
	//Arduino_Path ale
	//public static final String ARDUINO_COMMAND_PATH = "C:"+File.separator+"Program Files (x86)"+File.separator+"Arduino"+File.separator;
	//Path_luigi
	public static final String PATH_PREFIX ="\"D:"+File.separator+"Eclipse Workspaces"+File.separator+"mag workspace"+File.separator+"UnisannioWeather"+File.separator+"Server"+File.separator+"UnisannioWeatherServer"+File.separator;
	//Path_ale
	//public static final String PATH_PREFIX = "C:"+File.separator+"Users"+File.separator+"Alessandro"+File.separator+"Desktop"+File.separator+"Magistrale"+File.separator+"Eclipse"+File.separator+"unisannioweather"+File.separator+"Server"+File.separator+"UnisannioWeatherServer"+File.separator;
	public static final String LOAD_SKETCH_PATH = "Sketches"+File.separator+"load_sketch.bat"; 
	public static final String LDR_SKETCH_PATH = PATH_PREFIX+"Sketches"+File.separator+"Misura_luminosit_"+File.separator+"Misura_luminosit_.ino\"";
	public static final String BMP180_SKETCH_PATH = PATH_PREFIX+"Sketches"+File.separator+"Pressione"+File.separator+"Pressione.ino\"";
	public static final String DHT11_SKETCH_PATH = PATH_PREFIX+"Sketches"+File.separator+"Temperatura_umidit_"+File.separator+"Temperatura_umidit_.ino\"";
	public static final String EMPTY_SKETCH = PATH_PREFIX+"Sketches"+File.separator+"Empty_sketch"+File.separator+"Empty_sketch.ino\"";
	public static final String MYSQL_DB_URL = "jdbc:mysql://localhost:3306/unisannio_weather";
	public static final String MYSQL_DB_USERNAME = "root";
	public static final String MYSQL_DB_PASSWORD = "password";

	
	
}
