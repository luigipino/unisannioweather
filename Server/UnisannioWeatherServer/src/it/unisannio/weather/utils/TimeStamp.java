package it.unisannio.weather.utils;

import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.unisannio.weather.exceptions.TimeFormatException;

//This class represents the timeStamp of a Measure
//We establish a pattern for the time format : dd-MM-yyyy hh:mm
public class TimeStamp {

	private int dayOfMonth, month, year, hoursOfDay, minutes;
	private static final String STRING_TIME_STAMP_PATTERN = "(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d [01]?[0-9]|2[0-3]):[0-5][0-9]";

	private TimeStamp() {
		super();
		dayOfMonth = -1;
		month = -1;
		year = -1;
		hoursOfDay = -1;
		minutes = -1;
	}

	//Constructor from Calendar Object
	public TimeStamp(Calendar timeStamp) {
		dayOfMonth = timeStamp.get(Calendar.DAY_OF_MONTH);
		month = timeStamp.get(Calendar.MONTH)+1;
		year = timeStamp.get(Calendar.YEAR);
		hoursOfDay = timeStamp.get(Calendar.HOUR_OF_DAY);
		minutes = timeStamp.get(Calendar.MINUTE);
	}
	
	//Constructor from String
	//Basically used for the db reading
	public static TimeStamp getTimeStampFromString(String text) throws TimeFormatException{
		Pattern pattern = Pattern.compile(STRING_TIME_STAMP_PATTERN);
		Matcher m = pattern.matcher(text);
		if(m.matches()){
			StringTokenizer st = new StringTokenizer(text);
			String date = st.nextToken();
			String time = st.nextToken();
			StringTokenizer stDate = new StringTokenizer(date, "-");
			TimeStamp newTime = new TimeStamp();
			String day = stDate.nextToken();
			String month = stDate.nextToken();
			String year = stDate.nextToken();
			if(isCorrectDate(day, month, year)){
				newTime.setDayOfMonth(Integer.parseInt(day));
				newTime.setMonth(Integer.parseInt(month));
				newTime.setYear(Integer.parseInt(year));
				StringTokenizer sdTime = new StringTokenizer(time,":");
				newTime.setHoursOfDay(Integer.parseInt(sdTime.nextToken()));
				newTime.setMinutes(Integer.parseInt(sdTime.nextToken()));
				return newTime;
			}
		}
		throw new TimeFormatException("Illegal TimeStamp format!");
	}
	
	@Override
	public String toString() {
		//gg-mm-yyyy hh:mm
		String curDate = String.format("%02d-%02d-%04d", dayOfMonth, month, year);
		String curTime = String.format("%02d:%02d", hoursOfDay, minutes);
		return curDate+" "+curTime;
	}

	public int getDayOfMonth() {
		return dayOfMonth;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public int getHoursOfDay() {
		return hoursOfDay;
	}

	public int getMinutes() {
		return minutes;
	}

	/**
	 * @param dayOfMonth the dayOfMonth to set
	 */
	private void setDayOfMonth(int dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	/**
	 * @param month the month to set
	 */
	private void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @param year the year to set
	 */
	private void setYear(int year) {
		this.year = year;
	}

	/**
	 * @param hoursOfDay the hoursOfDay to set
	 */
	private void setHoursOfDay(int hoursOfDay) {
		this.hoursOfDay = hoursOfDay;
	}

	/**
	 * @param minutes the minutes to set
	 */
	private void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	
	//This method checks if the day are correct according to months and year
	private static boolean isCorrectDate(String day,String month,String year){
		int d = Integer.parseInt(day);
		int m = Integer.parseInt(month);
		int y = Integer.parseInt(year);
		switch(m){
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				return true;
			case 2:
				if((y % 400 == 0) || ((y % 4 == 0) && (y % 100 != 0))){ //Leap Year
						if(d<=29) return true;}
				else if(d<=28) return true;
				return false;
			case 4: case 6: case 9: case 11:
				if(d<=30) return true;
			default: return false;
		}
	}
}
