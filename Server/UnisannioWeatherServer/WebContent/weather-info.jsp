<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@page import="it.unisannio.weather.domain.places.City"%>
<%@page import="it.unisannio.weather.domain.measures.WeatherState"%>
<%@page import="it.unisannio.weather.domain.measures.Measure"%>
<%@page import="it.unisannio.weather.domain.measures.Attribute"%>
<%@page
	import="it.unisannio.weather.domain.measures.Attribute.AttributeName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>


<%
	City city = (City) request.getAttribute("city");
	WeatherState ws = city.getCurrentWeatherState();
	double temp = 0, humidity = 0, light = 0, pressure = 0;

	for (Measure m : ws.getMeasures()) {
		for (Attribute a : m.getAttributes()) {
			if (a.getName() == AttributeName.TEMPERATURE) {
				temp = a.getValue();
			} else if (a.getName() == AttributeName.HUMIDITY) {
				humidity = a.getValue();
			} else if (a.getName() == AttributeName.PRESSURE) {
				pressure = a.getValue();
			} else if (a.getName() == AttributeName.LIGHT) {
				light = a.getValue();
			}
		}
	}
%>
<title><%=city.getName()%> Weather</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500,900'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/ajaxtags.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/displaytag.css" />
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/prototype.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ajaxtags.js"></script>
</head>
<body>

	<!-- Header -->
	<div id="header"
		style="background:url(./images/<%=city.getName().replaceAll("\\s+", "")%>.jpg) no-repeat bottom center ;position: relative ;background-attachment:fixed ;background-size: cover; text-align: center;">
		<div id="nav-wrapper">
			<!-- Nav -->
			<nav id="nav">
			<ul>
				<li><a href="home.jsp">Home</a></li>

				<li></li>
				<li class="active"><a href="about-us.html">About us</a></li>
			</ul>
			</nav>
			<!-- /Nav -->
		</div>
		<!-- Container -->
		<div class="container">
			<!-- Logo -->
			<div id="logo">
				<h1>
					<a href="#"><%=city.getName().toString()%>�</a>
				</h1>
				<span class="tag"><%=request.getAttribute("region").toString()%></span>
				<br> <span class="tag"><%=String.valueOf(city.getAltitude()) + " m. on sea level"%></span>
			</div>
			<!-- /Logo -->
		</div>
		<!-- /Container -->
	</div>
	<!-- /Header -->

	<!-- Main -->
	<!-- Featured -->
	<div id="featured">
		<div class="container">

			<header>
			<h2>
				Last Measure:
				<%=ws.getTime().toString()%></h2>
			</header>
			<table width="400">
				<tr>
					<div class="row">
						<section class="4u"> <span class="pennant"><span
							class="wi wi-day-sunny"></span></span>
						<h3>
							<strong>Brightness</strong>
						</h3>
						<h3>
							<strong> <font size='30'> <%=String.valueOf(light)%>
									fc
							</font></strong>
						</h3>
						</section>
						<section class="4u"> <span class="pennant"><span
							class="wi wi-thermometer"></span></span>
						<h3>
							<strong>Temperature</strong>
						</h3>
						<h3>
							<strong> <font size='30'> <%=String.valueOf(temp)%>
									�C
							</font></strong>
						</h3>
						</section>
						<section class="4u"> <span class="pennant"><span
							class="fa fa-tint"></span></span>
						<h3>
							<strong>Humidity</strong>
						</h3>
						<h3>
							<strong> <font size='30'> <%=String.valueOf(humidity)%>
									%
							</font></strong>
						</h3>
						</section>
					</div>
				</tr>
				<tr>
					<div class="row">
						<section class="4u"> <span class="pennant"><span
							class="wi wi-sunsire"></span></span>
						<h3>
							<strong>Light hours</strong>
						</h3>
						<h3>
							<strong> <font size='30'> <%=city.getTodayLightHours()%>
							</font></strong>
						</h3>
						</section>
						<section class="4u"> <span class="pennant"><span
							class="fa fa-umbrella"></span></span>
						<h3>
							<strong>Rain odds</strong>
						</h3>
						<h3>
							<strong> <font size='30'><%=String.valueOf(ws.getRainOdds())%>
									% </font></strong>
						</h3>
						</section>

						<section class="4u"> <span class="pennant"><span
							class="wi wi-barometer"></span></span>
						<h3>
							<strong>Pressure</strong>
						</h3>
						<h3>
							<strong> <font size='30'> <%=String.valueOf(pressure)%>
									Pa
							</font></strong>
						</h3>
						</section>
					</div>
				</tr>
			</table>
		
			<br><a href="<%=request.getContextPath()%>/ArchiveServlet?city=<%=city.getName()+", "+request.getAttribute("region").toString()%>"
				class="button button-style1" >Go to archive</a>
				<!--  &region=request.getAttribute("region").toString() -->
		</div>
	</div>
	<!-- /Featured -->
	<!-- /Main -->

	<!-- Quote -->
	<div id="quote"
		style="background:url(./images/<%=city.getName().replaceAll("\\s+", "")%>.jpg) no-repeat center center;position: relative;
		text-align: center;
		background-attachment: fixed;
		background-size: cover;">
		<div class="container"></div>
	</div>
	<!-- /Quote -->
</body>
</html>