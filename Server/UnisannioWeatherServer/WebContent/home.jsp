<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Unisannio Weather</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500,900'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/ajaxtags.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/displaytag.css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/prototype.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ajaxtags.js"></script>
<noscript>
	<link rel="stylesheet" href="css/skel-noscript.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-desktop.css" />
</noscript>



</head>
<body class="homepage">
	<!-- Header -->
	<div id="header">
		<div id="nav-wrapper">
			<!-- Nav -->
			<nav id="nav">
			<ul>
				<li class="active"><a href="home.jsp">Home</a></li>
				<li><a href="about-us.html">About us</a></li>
				<!-- <li ></li> -->
			</ul>
			</nav>
		</div>
		<div class="container">

			<!-- Logo -->
			<div id="logo">
				<h1>
					<a>Unisannio Weather</a>
				</h1>
				<div class="container">
					<form name="search_city"
						action="${pageContext.request.contextPath}/WeatherServlet"
						method="GET">
						<input class="textField" id="search" type="text" name="city">
						<input type="submit" class="button button-style1" value="Search">>
						<%-- 			<button onclick="location.href='<%=request.getContextPath()%>/Request;'"></button>  --%>
						<ajax:autocomplete
							baseUrl="${pageContext.request.contextPath}/Autocomplete"
							source="search" target="search" className="autocomplete"
							parameters="input=search,city={search}" minimumCharacters="1" />
					</form>
				</div>
				<!-- /Logo -->
			</div>
		</div>
	</div>
	<!-- /Header -->
	<!-- Featured -->
	<div id="featured">
		<div class="container">
			<header>
			<h2>Welcome to Unisannio Weather</h2>
			</header>
			<p>
				The <strong>Unisannio Weather Station</strong> is a project focused
				on development of a weather system composed by several sensors
				displaced around the world. The goal is to provide a set of real
				environment measures, collected by these sensors, without claiming
				to produce weather forecast
			</p>
			<hr />
			<div class="row">
				<section class="4u"> <span class="pennant"><span
					class="fa fa-search-plus"></span></span>
				<h3>Historical Archive</h3>
				<p>Unisannio Weather provides the possibility to access all
					measurements of a given place</p>
				<a href="archive-home.jsp" class="button button-style1">Search
					in archive</a> </section>
				<section class="4u"> <span class="pennant"><span
					class="fa fa-bitbucket"></span></span>
				<h3>Source Code</h3>
				<p>
					See our source code and documentation project on <strong>
						Bitbucket.org </strong>
				</p>
				<a href="https://bitbucket.org/luigipino/unisannioweather/"
					target="_blank" class="button button-style1">Go to the project</a>
				</section>
				<section class="4u"> <span class="pennant"><span
					class="fa fa-cogs"></span></span>
				<h3>Hardware tools</h3>
				<p>Learn more informations about hardware tools used to realize
					a single weather station</p>
				<a href="${pageContext.request.contextPath}/hardware-tools.html"
					class="button button-style1">Read More</a> </section>
			</div>
		</div>
	</div>
	<!-- /Featured -->

	<!-- Quote -->
	<div id="quote">
		<div class="container">
			<section>
			<blockquote>&ldquo;I don't believe in pessimism. If
				something doesn't come up the way you want, forge ahead. If you
				think it's going to rain, it will&rdquo; - Clint Eastwood</blockquote>
			</section>
		</div>
	</div>
	<!--/Quote -->
	<!-- Footer -->
	<div align="center" id="footer">
		<header>
		<h2>Visit Number</h2>
		</header>
		<script type="text/javascript"
			src="http://counter2.statcounterfree.com/private/counter.js?c=d63f567252e24a99ee4af1f5f733a71b"></script>
	</div>
	<!-- /Footer -->
</body>
</html>