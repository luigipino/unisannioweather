<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=request.getAttribute("cityName")%> Archive</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500,900'
	rel='stylesheet' type='text/css'>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<noscript>
	<link rel="stylesheet" href="css/skel-noscript.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-desktop.css" />
	<script src="js/init.js"></script>
</noscript>
<%@page import="it.unisannio.weather.domain.measures.WeatherState"%>
<%@page import="it.unisannio.weather.domain.measures.Measure"%>
<%@page import="it.unisannio.weather.domain.measures.Attribute"%>
<%@page
	import="it.unisannio.weather.domain.measures.Attribute.AttributeName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

</head>
<body>

	<%
		List<WeatherState> wList = (List<WeatherState>) request.getAttribute("weatherList");
	%>
	<!-- Header -->
	<div id="header"
		style="background:url(./images/<%=((String) request.getAttribute("cityName")).replaceAll("\\s+", "")%>.jpg) no-repeat bottom center ;position: relative ;background-attachment:fixed ;background-size: cover; text-align: center;">
		<div id="nav-wrapper">
			<!-- Nav -->
			<nav id="nav">
			<ul>
				<li><a href="home.jsp">Home</a></li>

				<li></li>
				<li class="active"><a href="about-us.html">About us</a></li>
			</ul>
			</nav>
			<!-- /Nav -->
		</div>
		<!-- Container -->
		<div class="container">

			<!-- Logo -->
			<div id="logo">
				<h1>
					<a><%=request.getAttribute("cityName")%> Archive</a>
				</h1>
				<span class="tag"><%=request.getAttribute("region").toString()%></span>
				<br> <span class="tag"><%=String.valueOf(request.getAttribute("altitude")) + " m. on sea level"%></span>
			</div>
			<!-- /Logo -->
		</div>
		<!-- /Container -->
	</div>
	<!-- Header -->

	<!-- Main -->
	<!-- Featured -->
	<div id="featured">
		<div class="container">

			<header>
			<h2>All Measures</h2>
			</header>
			<hr />
			<fieldset>
				<table width="1240">
					<tr>
						<td><span class="pennant"><span class="fa fa-clock-o"></span></span>
							<h3>
								<strong><font size='5'>Measure Time </font></strong>
							</h3></td>
						<td><span class="pennant"><span
								class="wi wi-thermometer"></span></span>
							<h3>
								<strong><font size='5'>Temperature </font></strong>
							</h3></td>
						<td><span class="pennant"><span class="fa fa-tint"></span></span>
							<h3>
								<strong><font size='5'>Humidity </font></strong>
							</h3></td>
						<td><span class="pennant"><span class="fa fa-umbrella"></span></span>
							<h3>
								<strong><font size='5'>Rain odds </font></strong>
							</h3></td>
						<td><span class="pennant"><span
								class="wi wi-barometer"></span></span>
							<h3>
								<strong><font size='5'>Pressure</font></strong>
							</h3></td>
						<td><span class="pennant"><i class="wi wi-day-sunny">
							</i></span>
							<h3>
								<strong><font size='5'>Brightness</font></strong>
							</h3></td>


					</tr>
					<%
						double temp = 0, humidity = 0, light = 0, pressure = 0, rainOdds = 0;
						String time;
						for (WeatherState ws : wList) {
					%>
					<tr>
						<%
							time = ws.getTime().toString();
								rainOdds = ws.getRainOdds();
								for (Measure m : ws.getMeasures()) {
									for (Attribute a : m.getAttributes()) {
										if (a.getName() == AttributeName.TEMPERATURE) {
											temp = a.getValue();
										} else if (a.getName() == AttributeName.HUMIDITY) {
											humidity = a.getValue();
										} else if (a.getName() == AttributeName.PRESSURE) {
											pressure = a.getValue();
										} else if (a.getName() == AttributeName.LIGHT) {
											light = a.getValue();
										}
									}
								}
						%>
						<td>
							<h3>
								<font size='4'><%=time%></font>
							</h3>
						</td>
						<td>
							<h3>
								<font size='4'> <%=temp%> �C
								</font>
							</h3>
						</td>
						<td>
							<h3>
								<font size='4'> <%=humidity%> %
								</font>
							</h3>
						</td>
						<td>
							<h3>
								<font size='4'><%=rainOdds%> %</font>
							</h3>
						</td>
						<td>
							<h3>
								<font size='4'> <%=pressure%> Pa
								</font>
							</h3>
						</td>
						<td>
							<h3>
								<font size='4'><%=light%> Fc</font>
							</h3>
						</td>
					</tr>
					<%
						}
					%>
				</table>
			</fieldset>
		</div>
	</div>
	<!-- /Featured -->
	<!-- /Main -->

	<!-- Quote -->
	<div id="quote"
		style="background:url(./images/<%=((String) request.getAttribute("cityName")).replaceAll("\\s+", "")%>.jpg) no-repeat center center;position: relative;
		text-align: center;
		background-attachment: fixed;
		background-size: cover;">
		<div class="container">
			<section>
			<blockquote>&ldquo;I don't believe in pessimism. If
				something doesn't come up the way you want, forge ahead. If you
				think it's going to rain, it will&rdquo; - Clint Eastwood</blockquote>
			</section>
		</div>
	</div>
	<!-- /Quote -->

	<!-- Copyright -->
	<div id="copyright"></div>
	<!-- /Copyright -->

</body>
</html>