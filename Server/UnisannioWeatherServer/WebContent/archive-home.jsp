<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Archive</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500,900'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/ajaxtags.css" />
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/displaytag.css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/prototype.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ajaxtags.js"></script>
<noscript>
	<link rel="stylesheet" href="css/skel-noscript.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-desktop.css" />
</noscript>
</head>
<body class="homepage">

	<!-- Header -->
	<div id="header"
		style="background: url(./images/archive.jpg) no-repeat bottom center; position: relative; background-attachment: fixed; background-size: cover; text-align: center;">
		<div id="nav-wrapper">
			<!-- Nav -->
			<nav id="nav">
			<ul>
				<li class="active"><a href="home.jsp">Home</a></li>
				<li><a href="about-us.html">About us</a></li>
				<li></li>
			</ul>
			</nav>
			<!-- /Nav -->
		</div>
		<div class="container">
			<!-- Logo -->
			<div id="logo">
				<h1>
					<a>Unisannio Weather <br> Archive
					</a>
				</h1>
				<div class="container">
					<form name="search_city"
						action="${pageContext.request.contextPath}/ArchiveServlet"
						method="GET">
						<input class="textField" id="search" type="text" name="city">
						<input type="submit" class="button button-style1" value="Search">
						<%-- on click del bottone: location.href='<%=request.getContextPath()%>/Request' --%>
						<ajax:autocomplete
							baseUrl="${pageContext.request.contextPath}/Autocomplete"
							source="search" target="search" className="autocomplete"
							parameters="input=search,city={search}" minimumCharacters="1" />
					</form>
				</div>
			</div>
			<!-- /Logo -->
		</div>
	</div>
</body>
</html>